webpackJsonp([0],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackingPageModule", function() { return TrackingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tracking__ = __webpack_require__(369);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TrackingPageModule = /** @class */ (function () {
    function TrackingPageModule() {
    }
    TrackingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tracking__["a" /* TrackingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tracking__["a" /* TrackingPage */]),
            ],
        })
    ], TrackingPageModule);
    return TrackingPageModule;
}());

//# sourceMappingURL=tracking.module.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrackingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TrackingPage = /** @class */ (function () {
    function TrackingPage(apis, modalCtrl, navCtrl, navParams, loadingCtrl) {
        var _this = this;
        this.apis = apis;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.order_id = this.navParams.get('value');
        this.user_details = this.navParams.get("customer_details");
        console.log("ORDER ID" + this.order_id);
        this.kofi = "2";
        this.params = {
            "order_id": this.order_id
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.track_order(this.params).then(function (result) {
            console.log(result);
            _this.order_items = JSON.stringify(result);
            _this.status = result[0].status;
            _this.is_preparing = result[0].is_preparing;
            _this.on_way = result[0].on_way;
            _this.delivered = result[0].delivered;
            loader.dismiss();
        });
    }
    TrackingPage.prototype.refresh = function () {
        var _this = this;
        this.order_id = this.navParams.get('value');
        this.user_details = this.navParams.get("customer_details");
        console.log("ORDER ID" + this.order_id);
        this.kofi = "2";
        this.params = {
            "order_id": this.order_id
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.track_order(this.params).then(function (result) {
            console.log(result);
            _this.order_items = JSON.stringify(result);
            _this.status = result[0].status;
            _this.is_preparing = result[0].is_preparing;
            _this.on_way = result[0].on_way;
            _this.delivered = result[0].delivered;
            loader.dismiss();
        });
    };
    TrackingPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    TrackingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tracking',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/tracking/tracking.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Order Tracking <ion-icon name="refresh" class="right" tappable (click)="refresh()"></ion-icon> </ion-title>\n   \n    <!-- <ion-row>\n      <ion-col col-7>\n        <p>Order Tracking\n          </p>\n\n      </ion-col>\n      <ion-col col-5>\n        <ion-icon name="refresh" class="right" ></ion-icon> \n      </ion-col>\n    </ion-row>-->\n  </ion-navbar> \n\n</ion-header>\n\n\n<ion-content class="background">\n  <p text-wrap>\n    Thank you for ordering from\n    <b>M&F</b>\n  </p>\n\n  <h4 class="margup">\n    Track Order\n  </h4>\n\n  <ion-row>\n\n    <ion-col col-4 text-wrap>\n      <ion-item no-lines>\n        <!-- <b style="font-size: 100%;color: #000000;">10:00 AM</b>\n        <p style="font-size: 50%">Order Time</p> -->\n\n      </ion-item>\n    </ion-col>\n    <ion-col col-4 class="border" text-wrap>\n      <ion-item no-lines>\n\n        <b style="font-size: 100%;color: #000000;" text-wrap>Order ID</b>\n        <p style="font-size: 70%" text-wrap>{{order_id}}</p>\n\n        <!-- <img src="assets/icon/tracking_icon.png"> -->\n      </ion-item>\n    </ion-col>\n    <ion-col col-4 text-wrap>\n      <ion-item no-lines>\n        <!-- <b style="font-size: 100%;color: #000000;">10:30 AM</b>\n        <p style="font-size: 50%">Est Delivery Time</p> -->\n\n\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n\n\n\n  <div class="container" >\n    <ul class="progressbar">\n      <li  [class.active]= "status == true && is_preparing == false">Received\n          <div class="border1" *ngIf="status == true && is_preparing == false" >\n              <img src="assets/icon/order_received.png" width="20%"> <br>\n              <b style="font-size: 100%;color: #000000;">Order Received</b>\n              <p style="font-size: 70%">Your order has been received</p>\n      \n          </div>\n      </li>\n\n      <li [class.active]= "is_preparing == true && on_way == false" >Preparing\n          <div class="border2" *ngIf="is_preparing == true && on_way == false">\n              <img src="assets/icon/order_received.png" width="20%"> <br>\n              <b style="font-size: 100%;color: #000000;">Preparing Order</b>\n              <p style="font-size: 70%" text-wrap>Your order is being prepared for delivery</p>\n      \n          </div>\n      </li>\n      <li [class.active]= "on_way == true && delivered == fals">On the way\n          <div class="border2" *ngIf="on_way == true && delivered == false" >\n              <img src="assets/icon/order_received.png" width="20%"> <br>\n              <b style="font-size: 100%;color: #000000;">Yaayyy!</b>\n              <p style="font-size: 70%">Your order is on the way.</p>\n      \n          </div>\n      </li>\n      <li [class.active]= "delivered == true" >Delivered\n          <div class="border2" *ngIf="delivered == true" text-wrap>\n              <img src="assets/icon/order_received.png" width="20%"> <br>\n              <b style="font-size: 100%;color: #000000;" text-wrap>Enjoy your meal</b>\n              <p style="font-size: 70%" text-wrap>Your order has been delivered</p>\n      \n          </div>\n      </li>\n     \n    </ul> \n   \n  </div>\n\n\n  <p>\n      <button ion-button color="dark" class="button button-dark customBtn" (tap)="home()"> Continue Order </button>\n    </p>\n\n  \n\n\n</ion-content>\n<!-- <ion-footer >\n    <p>\n      <button ion-button color="dark" class="button button-dark customBtn" (tap)="home()"> Continue Order </button>\n    </p>\n  \n  </ion-footer> -->'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/tracking/tracking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */]])
    ], TrackingPage);
    return TrackingPage;
}());

//# sourceMappingURL=tracking.js.map

/***/ })

});
//# sourceMappingURL=0.js.map