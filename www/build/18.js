webpackJsonp([18],{

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPageModule", function() { return ListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__list__ = __webpack_require__(351);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ListPageModule = /** @class */ (function () {
    function ListPageModule() {
    }
    ListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__list__["a" /* ListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__list__["a" /* ListPage */]),
            ],
        })
    ], ListPageModule);
    return ListPageModule;
}());

//# sourceMappingURL=list.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListPage = /** @class */ (function () {
    function ListPage(toastCtrl, apis, _form, loadingCtrl, alertCtrl, navCtrl, navParams, http) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.apis = apis;
        this._form = _form;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.food_list = "Weekly";
        this.user_details = this.navParams.get("customer_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
            this.body2;
            console.log("Lenght " + this.body2);
        }
        else {
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            console.log("USer DETAILS From LOGIN" + this.user_details);
            console.log("USer ID " + this.user_id);
            var loader = this.loadingCtrl.create({
                content: "Please wait ...",
            });
            loader.present();
            this.params = {
                "user_id": this.user_id
            };
            this.apis.retrieve_list_details(this.params).then(function (result) {
                _this.body2 = result;
                _this.list_details = JSON.stringify(_this.body2);
                console.log("body2 is  " + _this.body2);
                console.log("body2  legnht is  " + _this.body2.length);
                console.log("Lets see all the restaurant_details " + _this.list_details);
                console.log("Lets see all the restaurant_details " + _this.list_details[0].resp_code);
                console.log("Lets see all the restaurant_details " + _this.list_details.resp_code);
                console.log("Lets see all the restaurant_details " + _this.list_details['resp_code']);
            });
            loader.dismiss();
        }
    }
    ListPage.prototype.createList = function () {
        this.navCtrl.push("CreatelistPage", { customer_details: this.user_details });
    };
    ListPage.prototype.placeOrder = function (item) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.params = {
            "user_id": this.user_id
        };
        this.apis.place_weekly_order(this.params).then(function (result) {
            _this.body2 = result;
            _this.list_details = JSON.stringify(_this.body2);
            console.log("Lets see all the restaurant_details as body " + _this.body2);
            console.log("Lenght " + _this.body2.length);
            console.log("Lets see all the restaurant_details " + _this.list_details);
        });
        loader.dismiss();
        var alert = this.alertCtrl.create({
            title: "",
            subTitle: "Your Weekly Order has been successfully placed. You will be contacted shortly by the M&F team.",
            buttons: ['OK']
        });
        alert.present();
    };
    ListPage.prototype.remove = function (item) {
        this.params = {
            "id": item.id
        };
        console.log("Lets ITEM " + item);
        console.log("Lets ITEM ID " + item.id);
        this.apis.delete_schedule(this.params).then(function (result) {
        });
        this.navCtrl.setRoot("ListPage", { customer_details: this.user_details });
        this.toastCtrl.create({
            message: "Item has been successfully removed.",
            duration: 5000
        }).present();
    };
    ListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Scheduled List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <ion-segment [(ngModel)]="food_list" overflow-scroll="true">\n    <ion-segment-button class="segment-button" value="Weekly">\n      Weekly\n    </ion-segment-button>\n    <!-- <ion-segment-button value="Monthly"> Monthly </ion-segment-button> -->\n  </ion-segment>\n\n  <div [ngSwitch]="food_list">\n    <ion-list *ngSwitchCase="\'Weekly\'" overflow-scroll="true">\n      <ion-card>\n        <ion-item>\n          <h2>\n            <b>Weekly Foodstuff List </b>\n          </h2>\n        </ion-item>\n      </ion-card>\n\n      <!-- <table>\n        <thead>\n          <th>Name</th>\n          <th>Price</th>\n        </thead>\n        <ion-item *ngFor="let item of body2">\n          <td>{{item.food_name}}</td>\n          <td>{{item.amount}}</td>\n          <td>\n            <button ion-button color="primary" outline (tap)="remove(item)">\n              Remove\n            </button>\n          </td>\n        </ion-item>\n      </table> -->\n\n      <ion-item *ngFor="let item of body2">\n        <ion-row>\n          <ion-col col-12 text-wrap>\n            <h4>Name: {{item.food_name}}</h4>\n            <p>Cost:<span style="color: #078016"> GHC {{item.amount}}</span></p>\n            <p>\n              <button ion-button color="primary" outline (tap)="remove(item)">\n                Remove\n              </button>\n            </p>\n          </ion-col>\n          <!-- <ion-col col-3 text-wrap>\n            <p>{{item.amount}}</p>\n          </ion-col> -->\n          <!-- <ion-col col-4>\n            <p>\n              <button ion-button color="primary" outline (tap)="remove(item)">\n                Remove\n              </button>\n            </p>\n          </ion-col> -->\n        </ion-row>\n      </ion-item>\n\n      <div class="center1" *ngIf="body2 == \'\'">\n        <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n        <p style="color: #757575; text-align: center">No List created</p>\n      </div>\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'Monthly\'" overflow-scroll="true">\n      <ion-card>\n        <ion-item>\n          <h2>\n            <b>Monthly Foodstuff List - GHC</b>\n          </h2>\n        </ion-item>\n      </ion-card>\n\n      <ion-item>\n        <div class="center1">\n          <p style="color: #757575; text-align: center">\n            Monthly Schedule not available\n          </p>\n        </div>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-row>\n    <ion-col col-6 text-wrap>\n      <p>\n        <button\n          ion-button\n          color="dark"\n          class="button button-dark customBtn"\n          (tap)="createList()"\n        >\n          Create List\n        </button>\n      </p>\n    </ion-col>\n    <ion-col col-6 text-wrap>\n      <p>\n        <button\n          ion-button\n          color="primary"\n          class="button button-dark customBtn"\n          (tap)="placeOrder(item)"\n          *ngIf="body2 != \'\'"\n        >\n          Place Order\n        </button>\n      </p>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/list/list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ })

});
//# sourceMappingURL=18.js.map