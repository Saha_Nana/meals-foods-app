webpackJsonp([16],{

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltyPageModule", function() { return LoyaltyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loyalty__ = __webpack_require__(353);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoyaltyPageModule = /** @class */ (function () {
    function LoyaltyPageModule() {
    }
    LoyaltyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__loyalty__["a" /* LoyaltyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__loyalty__["a" /* LoyaltyPage */]),
            ],
        })
    ], LoyaltyPageModule);
    return LoyaltyPageModule;
}());

//# sourceMappingURL=loyalty.module.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoyaltyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoyaltyPage = /** @class */ (function () {
    function LoyaltyPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.user_details = this.navParams.get("user_details");
        this.points = this.navParams.get("points");
        this.redeemable_amount = this.navParams.get("redeemable_amount");
        console.log("Points is  " + this.points);
    }
    LoyaltyPage.prototype.redeem = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: "Redeem Points",
            message: "You are about to redeem your loyalty points. Use your points in your next order",
            buttons: [
                {
                    text: "No",
                    handler: function () { },
                },
                {
                    text: "Continue",
                    handler: function () {
                        _this.navCtrl.setRoot("HomePage", { customer_details: _this.user_details });
                    }
                }
            ],
        });
        confirm.present();
    };
    LoyaltyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-loyalty',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/loyalty/loyalty.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Your MnF rewards points</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <div *ngIf="points">\n    <p class="middley">\n      You have\n      <span style="color: #ffc107; font-size: 40px"> {{points}}</span> MnF\n      rewards points\n    </p>\n\n    <ion-item no-lines class="center">\n      <img src="assets/imgs/online-shopping.svg" class="imgsize" />\n    </ion-item>\n\n    <div *ngIf="points < 10000">\n      <p class="middley">\n        You need a minimum points of 10000 to be able to redeem. Keep shopping!\n      </p>\n    </div>\n    <div class="move-but" *ngIf="points > 10000">\n      <button\n        ion-button\n        color="primary"\n        outline\n        icon-end\n        text-wrap\n        (tap)="redeem()"\n      >\n        Redeem Points for GHc50\n      </button>\n    </div>\n  </div>\n\n  <div class="center1" *ngIf="points == undefined || points == \' \'" text-wrap>\n    <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n    <p style="color: #757575; text-align: center">No Deals Available</p>\n  </div>\n\n  <div>\n    <img src="assets/imgs/bonanza.jpg" />\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/loyalty/loyalty.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], LoyaltyPage);
    return LoyaltyPage;
}());

//# sourceMappingURL=loyalty.js.map

/***/ })

});
//# sourceMappingURL=16.js.map