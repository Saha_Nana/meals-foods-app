webpackJsonp([21],{

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(348);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(actionSheetCtrl, modalCtrl, cartServ2, cartServ, toastCtrl, loadingCtrl, apis, event, alertCtrl, navCtrl, navParams, storage) {
        var _this = this;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.cartServ2 = cartServ2;
        this.cartServ = cartServ;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apis = apis;
        this.event = event;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.user_details = this.navParams.get("customer_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
            // let profileModal = this.modalCtrl.create("InfoPage");
            // profileModal.present();
            // let actionSheet = this.actionSheetCtrl.create({
            //   title: 'Important Notice',
            //   buttons: [
            //     {
            //       text: 'Read',
            //       role: 'Read',
            //       handler: () => {
            //         let profileModal = this.modalCtrl.create("InfoPage");
            //           profileModal.present();
            //         console.log('Read clicked');
            //       }
            //     },
            //     {
            //       text: 'Cancel',
            //       role: 'cancel',
            //       handler: () => {
            //         console.log('Cancel clicked');
            //       }
            //     }
            //   ]
            // });
            // actionSheet.present();
        }
        else {
            this.event.publish("customer_details", this.user_details);
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            this.first_name = this.jsonBody[0].first_name;
            console.log("USer DETAILS " + this.user_details);
            console.log("USer ID " + this.user_id);
            console.log("first_name" + this.first_name);
            // console.log("REMOVE CART");
            // this.cartServ.removeallcart();
            // this.cartServ2.removeallfoodcart();
            this.count = this.cartServ.getAllCartItems().length;
            this.params = {
                user_id: this.user_id,
            };
            var loader_1 = this.loadingCtrl.create({
                content: "Please wait ...",
                spinner: 'dots',
            });
            loader_1.present();
            //Retrieve Loyalty Points
            this.apis.retrieve_loyalty_points(this.params).then(function (result) {
                console.log(result);
                _this.points = result[0].points;
                _this.redeemable_amount = result[0].redeemable_amount;
                console.log("points,redeemable_amount " + _this.points, _this.redeemable_amount);
            });
            //END Retrieve Loyalty Points
            this.apis.order_history(this.params).then(function (result) {
                console.log(result);
                if (result["resp_code"] == "112") {
                    _this.toastCtrl
                        .create({
                        message: "No Meals Order History Available Yet",
                        duration: 3000,
                    })
                        .present();
                }
                else {
                    _this.order_date = result[0].order_date;
                    console.log("LETS SEE LIST order_date " + _this.order_date);
                }
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ["OK"],
                });
                alert.present();
                _this.toastCtrl
                    .create({
                    message: "Please check your internet connection",
                    duration: 5000,
                })
                    .present();
                loader_1.dismiss();
                console.log(err);
            });
            this.apis.food_order_history(this.params).then(function (result) {
                console.log(result);
                if (result["resp_code"] == "112") {
                    _this.toastCtrl
                        .create({
                        message: "No Orders Available yet",
                        duration: 3000,
                    })
                        .present();
                }
                else {
                    _this.food_order_date = result[0].order_date;
                    console.log("LETS SEE LIST food_order_date " + _this.food_order_date);
                }
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ["OK"],
                });
                alert.present();
                _this.toastCtrl
                    .create({
                    message: "Please check your internet connection",
                    duration: 5000,
                })
                    .present();
                loader_1.dismiss();
                console.log(err);
            });
            loader_1.dismiss();
        }
    }
    HomePage.prototype.notice = function () {
        var profileModal = this.modalCtrl.create("InfoPage");
        profileModal.present();
    };
    HomePage.prototype.catering = function () {
        this.navCtrl.push("SearchcateringPage", { user_details: this.user_details });
        // let alert = this.alertCtrl.create({
        //   title: "M&F",
        //   subTitle: "Coming Soon..",
        //   buttons: ["OK"],
        // });
        // alert.present();
    };
    HomePage.prototype.food = function () {
        this.navCtrl.push("FoodMenuPage", { user_details: this.user_details });
    };
    HomePage.prototype.meals = function () {
        this.navCtrl.push("SearchMealsPage", { user_details: this.user_details });
    };
    HomePage.prototype.loyalty = function () {
        this.navCtrl.push("LoyaltyPage", { user_details: this.user_details, points: this.points, redeemable_amount: this.redeemable_amount });
    };
    HomePage.prototype.check_orders = function () {
        this.navCtrl.push("OrderHistoryPage", {
            customer_details: this.user_details,
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-home",template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/home/home.html"*/'<!-- <ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Meals & Foodstuffs</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n\n  <p class="middley">Hi! What do you want to order today?</p>\n\n  <ion-grid>\n    <ion-row>\n\n      <ion-col col-6 class="middle">\n        <img src="assets/imgs/meal.png" class="imgsize" tappable (tap)="meals()" />\n        <figcaption>\n          <p style=" color: #000;">Meals</p>\n        </figcaption>\n      </ion-col>\n\n      <ion-col col-6 class="middle">\n        <img src="assets/imgs/food.png" class="imgsize" tappable (tap)="food()" />\n        <figcaption>\n          <p style=" color: #000;">Food Stuffs</p>\n        </figcaption>\n      </ion-col>\n\n\n    </ion-row>\n  </ion-grid>\n\n\n\n  <ion-row class="middlewhite">\n    <p class="left">My Orders</p>\n    <ion-col col-12>\n\n      <p *ngIf="order_date?.length > 0 || order_date?.length != null">Last Order placed on : {{order_date | date: \'medium\'}}</p>\n      <p>\n\n        <p *ngIf="order_date?.length == 0 || order_date?.length == null">No Order place yet</p>\n        <p>\n          <button ion-button color="primary" class="button button-dark customBtn" (click)="check_orders()"> Check Orders</button>\n        </p>\n\n    </ion-col>\n  </ion-row>\n\n</ion-content> -->\n\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Meals & Foodstuffs</ion-title>\n\n    <!-- <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="notice()">\n     <ion-icon name="alert" class="right"></ion-icon>\n\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <p class="middley">Hi {{first_name}}! What do you want to order today?</p>\n  <p class="middley" *ngIf="points">\n    You have <span style="color: #ffc107"> {{points}}</span> MnF rewards points\n  </p>\n\n  <!-- <ion-list class="main-view"> -->\n  <br />\n\n  <!-- <div class="paddup">\n      <p>\n        <button\n          ion-button\n          color="primary"\n          class="button button-dark customBtn1"\n          tappable\n          (tap)="food()"\n        >\n          FOODSTUFFS\n        </button>\n      </p>\n      <p>\n        <button\n          ion-button\n          color="secondary"\n          class="button button-dark customBtn1"\n          tappable\n          (tap)="meals()"\n        >\n          MEALS\n        </button>\n      </p>\n    </div> -->\n  <!-- </ion-list> -->\n\n  <div class="direct_service_row">\n    <div class="column3">\n      <div class="border" tappable (tap)="meals()">\n        <img src="assets/imgs/catering.svg" class="img_size" />\n\n        <figcaption>\n          <b id="txtcolor">Meals</b>\n        </figcaption>\n      </div>\n    </div>\n\n    <div class="column4">\n      <div class="border" tappable (tap)="food()">\n        <img src="assets/imgs/groce3.png" class="img_size" />\n        <figcaption>\n          <b id="txtcolor">Foodstuffs</b>\n        </figcaption>\n      </div>\n    </div>\n\n    <div class="column5">\n      <div class="border" tappable (tap)="catering()">\n        <img src="assets/imgs/buffet.svg" class="img_size" />\n        <figcaption>\n          <b id="txtcolor">Catering Services</b>\n        </figcaption>\n      </div>\n    </div>\n\n    <div class="column6">\n      <div class="border" tappable (tap)="loyalty()">\n        <img src="assets/imgs/online-shopping.svg" class="img_size" />\n        <figcaption>\n          <b id="txtcolor">Deals</b>\n        </figcaption>\n      </div>\n    </div>\n  </div>\n\n  <div class="section_2">\n    <div class="activi">\n      <p class="fboldness">Recent Activities</p>\n    </div>\n\n    <ion-list>\n      <ion-card>\n        <ion-item>\n          <ion-row>\n            <ion-col col-2>\n              <ion-avatar item-start>\n                <img src="assets/imgs/shopping.svg" />\n              </ion-avatar>\n            </ion-col>\n            <ion-col col-5>\n              <h2 class="fboldness" text-wrap>Foodstuff Orders:</h2>\n              <p\n                *ngIf="food_order_date?.length > 0 || food_order_date?.length != null"\n                text-wrap\n              >\n                Last Order placed on : {{food_order_date | date: \'medium\'}}\n              </p>\n              <p\n                *ngIf="food_order_date?.length == 0 || food_order_date?.length == null"\n                text-wrap\n              >\n                No Order placed yet\n              </p>\n            </ion-col>\n            <ion-col col-5 text-wrap>\n              <button\n                ion-button\n                color="primary"\n                outline\n                icon-end\n                (click)="check_orders()"\n                text-wrap\n              >\n                Check Orders\n\n                <!-- <ion-icon name="trending-up"></ion-icon> -->\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </ion-card>\n\n      <ion-card>\n        <ion-item>\n          <ion-row>\n            <ion-col col-2>\n              <ion-avatar item-start>\n                <img src="assets/imgs/back.svg" />\n              </ion-avatar>\n            </ion-col>\n            <ion-col col-5>\n              <h2 class="fboldness" text-wrap>Meals Orders:</h2>\n              <p\n                *ngIf="order_date?.length > 0 || order_date?.length != null"\n                text-wrap\n              >\n                Last Order placed on : {{order_date | date: \'medium\'}}\n              </p>\n              <p\n                *ngIf="order_date?.length == 0 || order_date?.length == null"\n                text-wrap\n              >\n                No Order placed yet\n              </p>\n            </ion-col>\n            <ion-col col-5>\n              <button\n                ion-button\n                color="secondary"\n                outline\n                icon-end\n                (click)="check_orders()"\n                text-wrap\n              >\n                Check Orders\n\n                <!-- <ion-icon name="trending-up"></ion-icon> -->\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </ion-card>\n    </ion-list>\n  </div>\n</ion-content>\n\n<!-- <ion-footer>\n  <div class="ash">\n    <p class="middlewhite2">My Orders</p>\n\n    <h4 class="middle">Foodstuff Orders:</h4>\n    <p\n      class="middle"\n      *ngIf="food_order_date?.length > 0 || food_order_date?.length != null"\n    >\n      Last Order placed on : {{food_order_date | date: \'medium\'}}\n    </p>\n    <p\n      class="middle"\n      *ngIf="food_order_date?.length == 0 || food_order_date?.length == null"\n    >\n      No Order placed yet\n    </p>\n\n    <h4 class="middle">Meals Orders:</h4>\n    <p\n      class="middle"\n      *ngIf="order_date?.length > 0 || order_date?.length != null"\n    >\n      Last Order placed on : {{order_date | date: \'medium\'}}\n    </p>\n    <p\n      class="middle"\n      *ngIf="order_date?.length == 0 || order_date?.length == null"\n    >\n      No Order placed yet\n    </p>\n\n    <p>\n      <button\n        ion-button\n        color="secondary"\n        class="button button-dark customBtn"\n        (click)="check_orders()"\n      >\n        Check Orders\n      </button>\n    </p>\n  </div>\n</ion-footer> -->\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["b" /* CartService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=21.js.map