webpackJsonp([5],{

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestaurantsPageModule", function() { return RestaurantsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__restaurants__ = __webpack_require__(364);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RestaurantsPageModule = /** @class */ (function () {
    function RestaurantsPageModule() {
    }
    RestaurantsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__restaurants__["a" /* RestaurantsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__restaurants__["a" /* RestaurantsPage */]),
            ],
        })
    ], RestaurantsPageModule);
    return RestaurantsPageModule;
}());

//# sourceMappingURL=restaurants.module.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RestaurantsPage = /** @class */ (function () {
    function RestaurantsPage(apis, loadingCtrl, alertCtrl, navCtrl, navParams) {
        var _this = this;
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fakeUsers = new Array(5);
        this.location_id = this.navParams.get("location_id_value");
        this.location_name = this.navParams.get("location_name");
        this.user_details = this.navParams.get("user_details");
        console.log('Location ID retrieved:', this.location_id);
        this.apis.fetch_restaurants(this.location_id).then(function (result) {
            _this.body = result;
            _this.restaurant_details = JSON.stringify(_this.body);
            console.log("Lets see all the restaurant_details as body " + _this.body);
            console.log("Lets see all the restaurant_details " + _this.restaurant_details);
        });
    }
    RestaurantsPage.prototype.openMenu = function (item) {
        console.log("Lets restaurant id " + JSON.stringify(item.id));
        this.navCtrl.push("MealMenuPage", { restaurant_id: item.id, user_details: this.user_details });
    };
    RestaurantsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restaurants',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/restaurants/restaurants.html"*/'<ion-header color="ash">\n  <ion-navbar color="ash">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-row>\n      <ion-col col-12>\n        <p>\n          Restaurant Location <br />\n          {{location_name}}\n        </p>\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <ion-list-header> Available Restaurants close to you </ion-list-header>\n\n  <div class="center1" *ngIf="body?.length == 0">\n    <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n    <p style="color: #757575; text-align: center">\n      No restaurants can be found in this location\n    </p>\n  </div>\n\n  <ion-list no-lines *ngIf="body">\n    <ion-list *ngFor="let item of body">\n      <ion-card tappable (tap)="openMenu(item)">\n        <ion-item>\n          <ion-avatar item-start>\n            <img [src]="item.logo" />\n          </ion-avatar>\n\n          <h2 text-wrap>\n            <b\n              >{{ item.name}}\n              <ion-badge\n                class="badge"\n                *ngIf=" item.restaurants_time_status == true"\n                >Open</ion-badge\n              >\n              <ion-badge\n                class="badge"\n                id="badgecolor"\n                *ngIf=" item.restaurants_time_status == false"\n                >Closed</ion-badge\n              >\n            </b>\n          </h2>\n          <p text-wrap>{{ item.descriptions}}</p>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star-half" class="star"></ion-icon>\n          <br />\n\n          <p class="right" text-wrap>Delivers in {{ item.delivery_estimate}}</p>\n        </ion-item>\n      </ion-card>\n      <br />\n    </ion-list>\n  </ion-list>\n\n  <ion-list *ngIf="!body">\n    <ion-item *ngFor="let fake of fakeUsers" class="fakeItem">\n      <ion-card>\n        <ion-item>\n          <ion-avatar item-start>\n            <img [src]="" />\n          </ion-avatar>\n\n          <h2></h2>\n          <p></p>\n          <br />\n          <p></p>\n\n          <p class="right"></p>\n        </ion-item>\n      </ion-card>\n      <br />\n      <br />\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/restaurants/restaurants.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], RestaurantsPage);
    return RestaurantsPage;
}());

//# sourceMappingURL=restaurants.js.map

/***/ })

});
//# sourceMappingURL=5.js.map