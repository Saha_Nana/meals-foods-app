webpackJsonp([24],{

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodPaymentPageModule", function() { return FoodPaymentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_payment__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular4_paystack__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FoodPaymentPageModule = /** @class */ (function () {
    function FoodPaymentPageModule() {
    }
    FoodPaymentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_payment__["a" /* FoodPaymentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_payment__["a" /* FoodPaymentPage */]),
                __WEBPACK_IMPORTED_MODULE_3_angular4_paystack__["a" /* Angular4PaystackModule */]
            ],
        })
    ], FoodPaymentPageModule);
    return FoodPaymentPageModule;
}());

//# sourceMappingURL=food-payment.module.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodPaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FoodPaymentPage = /** @class */ (function () {
    function FoodPaymentPage(toastCtrl, _form, navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this._form = _form;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.public_key = 'pk_live_25b093361d8ff68330d94fda3baefea56b0db1c4'; //Put your paystack Test or Live Key here
        this.channels = ['bank', 'card', 'ussd', 'qr', 'mobile_money']; //Paystack Payment Methods
        this.random_id = Math.floor(Date.now() / 1000); //Line to generate reference number
        this.user_details = this.navParams.get("user_details");
        this.order_id = this.navParams.get("order_id");
        this.SpecialRequest = this.navParams.get("requests");
        this.NewLocation = this.navParams.get("new_location");
        this.total_amount_plus_delivery = this.navParams.get("total_amount_plus_delivery") * 100;
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        this.customer_name = this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name;
        this.customer_email = this.jsonBody[0].email;
        this.mobile_number = this.jsonBody[0].mobile_number;
        console.log("USer DETAILS " + this.user_details);
        console.log("USer ID " + this.user_id);
        console.log("mobile_number " + this.mobile_number);
        console.log("order_id " + this.order_id);
        console.log('SpecialRequest IS' + this.SpecialRequest);
        console.log('NewLocation IS' + this.NewLocation);
        console.log('total_amount_plus_delivery IS' + this.total_amount_plus_delivery);
        this.amount = this.cartServ.getGrandTotal();
        console.log('AMOUNT IS' + this.amount);
        //  this.total_amt = parseFloat(this.amount + ((this.amount * 0.10) + 15 )).toFixed(2);
        //  console.log("totl amount is " + this.total_amt )  
        this.cartList = cartServ.getAllCartfoodItems();
        this.paymentForm = this._form.group({
            "momo_number": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            "network": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            "voucher": [""],
        });
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        //params for special requests
        this.params2 = {
            "order_id": this.order_id,
            "requests": this.SpecialRequest,
            "new_location": this.NewLocation
        };
        this.apis.save_special_request_food(this.params2).then(function (result) {
            console.log(result);
        });
        this.apis.retrieve_delivery_charges().then(function (result) {
            _this.body = result;
            console.log(_this.body);
            console.log(JSON.stringify(_this.body));
            console.log("Information", _this.body);
            _this.service_fee = _this.body[0].service_fee;
            _this.delivery_fee = _this.body[0].delivery_fee;
            console.log("------------SERV FEE _+ DELIVE-------------------------------------------");
            console.log("SERVICE FEE" + _this.service_fee);
            console.log("DEL FEE " + _this.delivery_fee);
            _this.total_amt = _this.getTotal() + (_this.getTotal() * _this.service_fee) + _this.delivery_fee;
            console.log("TOTAL AMOUNT PLUS DELIVERY" + _this.total_amt);
        });
        loader.dismiss();
    }
    FoodPaymentPage.prototype.getTotal = function () {
        this.amount = this.cartServ.getGrandTotal();
        return this.cartServ.getGrandTotal();
    };
    FoodPaymentPage.prototype.pay = function () {
        var _this = this;
        this.formValue = JSON.stringify(this.paymentForm.value);
        this.jsonBody = JSON.parse(this.formValue);
        console.log("FORM VALUE " + this.formValue);
        console.log("USer ID " + this.user_id);
        console.log("ORDER ID " + this.order_id);
        console.log("customer_name " + this.customer_name);
        console.log("customer_email " + this.customer_email);
        console.log("mobile_number " + this.mobile_number);
        console.log("MOMO number " + this.jsonBody.momo_number);
        console.log("MOMO network " + this.jsonBody.network);
        console.log("MOMO voucher " + this.jsonBody.voucher);
        console.log("Amount " + this.total_amt);
        this.params = {
            "customer_name": this.customer_name,
            "customer_email": this.customer_email,
            "customer_telephone": this.mobile_number,
            "mobile_wallet_number": this.jsonBody.momo_number,
            "mobile_wallet_network": this.jsonBody.network,
            "amount": this.total_amt,
            "user_id": this.user_id,
            "order_id": this.order_id,
            "voucher": this.jsonBody.voucher
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.make_payment(this.params).then(function (result) {
            console.log("------------RESULTS--------------");
            console.log(result);
            loader.dismiss();
            _this.navCtrl.setRoot("ReceiptPage", { momo_network: _this.jsonBody.network, order_id: _this.order_id, user_details: _this.user_details, total_amount: _this.total_amt });
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: "",
                subTitle: "Ooops! We encountered an issue trying to charge the mobile wallet. Please try again!",
                buttons: ['OK']
            });
            alert.present();
            _this.toastCtrl.create({
                message: "Please check your internet connection",
                duration: 5000
            }).present();
            loader.dismiss();
            console.log(err);
        });
    };
    FoodPaymentPage.prototype.cash_pay = function () {
        console.log("totl amount is " + this.total_amt);
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.navCtrl.setRoot("ReceiptPage", { momo_network: "", order_id: this.order_id, user_details: this.user_details, total_amount: this.total_amt });
        loader.dismiss();
    };
    //Callback function on successful payment 
    FoodPaymentPage.prototype.paymentDone = function (ref) {
        console.log(ref); //ref contains the response from paystack after successful payment
        this.navCtrl.push("ReceiptPage", { momo_network: "", order_id: this.order_id, user_details: this.user_details });
    };
    //Event triggered if User cancel the payment
    FoodPaymentPage.prototype.paymentCancel = function () {
        console.log('gateway closed');
        //  this.navCtrl.push("ReceiptPage", { momo_network: "", order_id: this.order_id, user_details: this.user_details })
    };
    FoodPaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-payment',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-payment/food-payment.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Pay</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <ion-item no-lines>\n    <h2>Pay with</h2>\n  </ion-item>\n\n  <ion-list radio-group [(ngModel)]="money">\n    <ion-item no-lines>\n      <angular4-paystack\n        [key]="\'pk_live_6e429e6998e18b9ecdc9c2d42e884dee6da9d352\'"\n        [email]="\'mnf@gmail.com\'"\n        [amount]="total_amount_plus_delivery"\n        [ref]="random_id"\n        [channels]="channels"\n        [currency]="\'GHS\'"\n        [class]="\'btn btn-primary\'"\n        (close)="paymentCancel()"\n        (callback)="paymentDone($event)"\n        >Pay with Mobile Money/Card</angular4-paystack\n      >\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-label> Cash on delivery</ion-label>\n      <ion-radio value="cash"></ion-radio>\n    </ion-item>\n\n    <ion-card>\n      <ion-card-header> Alternative Mobile Money Payment</ion-card-header>\n\n      <ion-card-content>\n        Payment instructions for those outside Ghana.\n\n        <ul class="pricing-plan__list">\n          <li class="pricing-plan__feature">\n            Send Mobile Money to\n            <span style="color: #781318; font-weight: 800">0552967435 </span>\n          </li>\n          <li class="pricing-plan__feature">\n            M&F Momo Account name:\n            <span style="color: #781318; font-weight: 800"\n              >Meals And Foodstuff Limited</span\n            >\n            (Note if your money transfer app requires first and last name, use\n            first name: Meals, last name: Foodstuffs Limited .\n          </li>\n          <li class="pricing-plan__feature">\n            Send WhatsApp text to\n            <span style="color: #781318; font-weight: 800">233552967435 </span>\n            to let M&F know you made the payment and request for acknowledgment\n            of payment receipt.\n          </li>\n        </ul>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n\n  <ion-list *ngIf="money == \'momo\'">\n    <ion-item>\n      <p><b>Complete the details and make payment</b></p>\n    </ion-item>\n\n    <form [formGroup]="paymentForm">\n      <ion-item no-lines>\n        <h2>Mobile money number</h2>\n      </ion-item>\n\n      <ion-item>\n        <ion-input\n          type="number"\n          placeholder="Enter your mobile money number"\n          formControlName="momo_number"\n          name="momo_number"\n          [class.invalid]="!paymentForm.controls.momo_number.valid && (paymentForm.controls.momo_number.dirty || submitAttempt)"\n        ></ion-input>\n      </ion-item>\n\n      <ion-item no-lines>\n        <h2>Select Mobile Network</h2>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>Mobile Network</ion-label>\n        <ion-select\n          formControlName="network"\n          name="network"\n          [class.invalid]="!paymentForm.controls.network.valid && (paymentForm.controls.network.dirty || submitAttempt)"\n        >\n          <ion-option\n            formControlName="network"\n            name="network"\n            value="mtn"\n            ngDefaultControl\n            >Mtn</ion-option\n          >\n          <ion-option\n            formControlName="network"\n            name="network"\n            value="tigo"\n            ngDefaultControl\n          >\n            Tigo</ion-option\n          >\n          <ion-option\n            formControlName="network"\n            name="network"\n            value="vodafone"\n            ngDefaultControl\n            >Vodafone</ion-option\n          >\n          <ion-option\n            formControlName="network"\n            name="network"\n            value="airtel"\n            ngDefaultControl\n            >Airtel</ion-option\n          >\n        </ion-select>\n      </ion-item>\n\n      <ion-item *ngIf="paymentForm.controls.network.value == \'vodafone\'">\n        <ion-input\n          type="text"\n          placeholder="Enter Vodafone Voucher"\n          formControlName="voucher"\n          name="voucher"\n          [class.invalid]="!paymentForm.controls.voucher.valid && (paymentForm.controls.voucher.dirty || submitAttempt)"\n        ></ion-input>\n      </ion-item>\n      <ion-card *ngIf="paymentForm.controls.network.value == \'vodafone\'">\n        <ion-item>\n          <b text-wrap> Notice</b><br />\n          <p text-wrap>1. Dial *110#</p>\n          <br />\n          <p text-wrap>2. Choose Option 6 to generate your payment Voucher</p>\n          <br />\n          <p text-wrap>\n            3. You will recieve an SMS with a 6 digit Voucher Code\n          </p>\n          <br />\n          <p text-wrap>4. Enter the Voucher Code in the voucher code input</p>\n          <br />\n          <p text-wrap>5. Initiate Payment by tapping the Pay button</p>\n          <br />\n        </ion-item>\n      </ion-card>\n\n      <ion-card *ngIf="paymentForm.controls.network.value == \'mtn\'">\n        <ion-item>\n          <b text-wrap> Notice</b><br />\n\n          <p text-wrap>\n            Before making payment, please make sure you have no pending payment\n            approval request\n          </p>\n        </ion-item>\n      </ion-card>\n    </form>\n  </ion-list>\n  <br />\n  <p *ngIf="money == \'momo\'">\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      [disabled]="!this.paymentForm.valid"\n      (tap)="pay()"\n    >\n      Pay GHS {{total_amount_plus_delivery}}\n    </button>\n  </p>\n  <p *ngIf="money == \'cash\'">\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      (tap)="cash_pay()"\n    >\n      Place Order\n    </button>\n  </p>\n</ion-content>\n\n<!-- <ion-footer >\n    <p *ngIf="money == \'momo\'">\n        <button ion-button color="secondary" class="button button-dark customBtn" (tap)="pay()">Pay GHS {{getTotal() + 10}}</button>\n       \n      </p>\n  <p *ngIf="money == \'cash\'">\n    <button ion-button color="secondary" class="button button-dark customBtn" (tap)="pay()">Place Order</button>\n  </p>\n</ion-footer> -->\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-payment/food-payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FoodPaymentPage);
    return FoodPaymentPage;
}());

//# sourceMappingURL=food-payment.js.map

/***/ })

});
//# sourceMappingURL=24.js.map