webpackJsonp([1],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlidersPageModule", function() { return SlidersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sliders__ = __webpack_require__(368);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SlidersPageModule = /** @class */ (function () {
    function SlidersPageModule() {
    }
    SlidersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sliders__["a" /* SlidersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sliders__["a" /* SlidersPage */]),
            ],
        })
    ], SlidersPageModule);
    return SlidersPageModule;
}());

//# sourceMappingURL=sliders.module.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlidersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SlidersPage = /** @class */ (function () {
    function SlidersPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.splash = true;
    }
    //    public ngOnInit() {
    //         let swiperContainer = this._elementRef.nativeElement.getElementsByClassName('swiper-container')[0];
    //         waitRendered(swiperContainer).then(()=>{
    //             let swiper = this._slider;
    //             swiper.update();
    //         });
    // }
    SlidersPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () { return _this.splash = false; }, 4000);
        console.log('ionViewDidLoad SliderPage');
    };
    SlidersPage.prototype.skip = function () {
        // this.navCtrl.setRoot("LoginPage")
        this.navCtrl.setRoot("HomePage");
    };
    SlidersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-sliders',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/sliders/sliders.html"*/'<!-- <ion-content class="background" >\n  <ion-slides autoplay="1000" loop="true" speed="2000" autoplayDisableOnInteraction="false" zoom="true" pager>\n\n\n    <ion-slide class="background">\n      <img src="assets/imgs/chef.svg"/> \n      \n      <p style=" color: #333333; font-weight: 500" text-wrap>Your favorite Restaurants ready to receive your orders</p>\n      <br>\n      <div class="card-title" tappable (tap)="skip()">\n        <ion-icon tappable name="md-arrow-forward"></ion-icon>\n      </div>\n\n    </ion-slide>\n\n\n    <ion-slide class="background">\n     <img src="assets/imgs/undraw.svg"  /> \n      \n      <p style=" color: #333333; font-weight: 500" text-wrap>Any meal you want? We got you served</p>\n      <br>\n      <div class="card-title" tappable (tap)="skip()">\n        <ion-icon tappable name="md-arrow-forward"></ion-icon>\n      </div>\n\n    </ion-slide>\n\n    <ion-slide class="background">\n       <img src="assets/imgs/delivery.svg"  /> \n     \n      <p style=" color: #333333; font-weight: 500" text-wrap>Get your food delivered to you instantly!</p>\n      <br>\n      <div class="card-title" tappable (tap)="skip()">\n        <ion-icon  tappable name="md-arrow-forward"></ion-icon>\n      </div>\n    </ion-slide>\n\n\n    <ion-slide class="background">\n      <img src="assets/imgs/pay.svg" /> \n    \n      <p style=" color: #333333; font-weight: 500" text-wrap>Choose between a number of payment options!</p>\n      <br>\n      <div class="card-title" tappable (tap)="skip()">\n        <ion-icon tappable name="md-arrow-forward"></ion-icon>\n      </div>\n\n    </ion-slide>\n\n\n  </ion-slides>\n\n</ion-content> -->\n\n<!-- <div id="custom-overlay" [style.display]="splash ? \'flex\': \'none\'">\n  <div class="flb">\n    <div class="Aligner-item Aligner-item--top"></div>\n  \n    <img src="assets/imgs/logo.png">\n  \n    <div class="Aligner-item Aligner-item--bottom"></div>\n  </div>\n</div> -->\n\n<ion-content overflow-scroll="true" class="background2">\n  <div class="move_text_down">\n    <h2>\n      <b style="color: white"> Your Meals! Your Groceries! </b>\n    </h2>\n    <p text-wrap style="color: white">\n      Get all your groceries & meals from your favorite restaurants delivered\n      instantly without any hassle!\n    </p>\n\n    <div class="padding">\n      <b class="pad">\n        <button\n          ion-button\n          color="primary"\n          class="button button-dark customBtn"\n          (tap)="skip()"\n        >\n          <b>Get Started</b>\n        </button>\n      </b>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/sliders/sliders.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["q" /* NavParams */]])
    ], SlidersPage);
    return SlidersPage;
}());

//# sourceMappingURL=sliders.js.map

/***/ })

});
//# sourceMappingURL=1.js.map