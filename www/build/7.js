webpackJsonp([7],{

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReceiptPageModule", function() { return ReceiptPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__receipt__ = __webpack_require__(362);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReceiptPageModule = /** @class */ (function () {
    function ReceiptPageModule() {
    }
    ReceiptPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__receipt__["a" /* ReceiptPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__receipt__["a" /* ReceiptPage */]),
            ],
        })
    ], ReceiptPageModule);
    return ReceiptPageModule;
}());

//# sourceMappingURL=receipt.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceiptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReceiptPage = /** @class */ (function () {
    function ReceiptPage(navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl, cartServ2) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.cartServ2 = cartServ2;
        this.order_id = this.navParams.get("order_id");
        this.user_details = this.navParams.get("user_details");
        this.momo_network = this.navParams.get("momo_network");
        this.total_amount = this.navParams.get("total_amount");
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        console.log("USer DETAILS " + this.user_details);
        console.log("USer ID " + this.user_id);
        console.log("momo_network " + this.momo_network);
        console.log("total_amount " + this.total_amount);
        console.log("REMOVE CART");
        this.cartServ.removeallcart();
        this.cartServ2.removeallfoodcart();
        this.cartList = cartServ.getAllCartItems();
        console.log(this.cartList);
    }
    ReceiptPage.prototype.getTotal = function () {
        this.check = this.cartServ.getGrandTotal();
        return this.cartServ.getGrandTotal();
    };
    ReceiptPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    ReceiptPage.prototype.order_history = function () {
        this.navCtrl.push("OrderHistoryPage", { customer_details: this.user_details });
        // let alert = this.alertCtrl.create({
        //       title: "Oopps!",
        //       subTitle: "Feature not available",
        //       buttons: ['OK']
        //     });
        //     alert.present();
    };
    ReceiptPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-receipt',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/receipt/receipt.html"*/'\n\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Receipt</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="background" >\n\n    <ion-list *ngIf="momo_network == \'mtn\'">\n        <ion-item no-lines class="center" text-wrap>\n            <h3><b> Payment Request Successfully Sent</b></h3><br>\n            <p>You have 60 seconds to authorize payment on your mobile money handset</p>\n            \n          </ion-item>\n        <ion-item no-lines class="center">\n            <img src="assets/imgs/phone.svg" class="imgsize"  />\n        </ion-item>\n          \n       <ion-card>\n          <ion-item no-lines class="center">      \n              <b text-wrap> Please follow the instructions below to authorize payment</b><br>\n              <p text-wrap> 1. Dial *170# on your mobile handset</p> <br>\n              <p text-wrap> 2. Choose Option 6 [My Wallet]</p><br>\n              <p text-wrap>3. Choose Option 3 [My Approvals]</p><br>\n              <p text-wrap>4. Enter your MOMO pin</p><br>\n              <p text-wrap>5. Choose the pending transaction</p> <br>\n              <p text-wrap>6. Choose Option 1 to approve</p><br>\n            </ion-item>\n          </ion-card>\n\n      </ion-list>\n\n\n  <ion-list *ngIf="momo_network == \'tigo\' || momo_network == \'airtel\' || momo_network == \'vodafone\'">\n    <ion-item no-lines class="center" text-wrap>\n        <p><b> Congratulations!!</b> <br>Your Order has been received!</p><br>\n        <h3 text-wrap ><b> A payment request was sent successfully to your mobile money number. Kindly complete & authorize the payment. </b></h3><br>\n        \n      </ion-item>\n    <ion-item no-lines class="center">\n        <img src="assets/imgs/phone.svg" class="imgsize"  />\n      </ion-item>\n\n      <!-- <ion-item no-lines class="center" text-wrap>\n          \n          <p>  Total Order is GHc  {{getTotal() + 10}}</p>\n          <p> Order ID is {{order_id}}</p>\n        </ion-item> -->\n      </ion-list>\n    \n      <ion-list *ngIf="momo_network == \'\'">\n          <ion-item no-lines class="center" text-wrap>\n              <p> Congratulations!! <br>Your Order has been received!</p>\n              \n            </ion-item>\n          <ion-item no-lines class="center">\n              <img src="assets/imgs/invoice2.svg" class="imgsize"  />\n            </ion-item>\n      \n            <ion-item no-lines class="center">\n                \n                <p>  Total Order is GHc  {{total_amount | number:\'1.2-2\'}}</p>\n                <p> Order ID is {{order_id}}</p>\n              </ion-item>\n        </ion-list>\n        \n        \n\n</ion-content>\n\n<ion-footer>\n    <div >\n\n      <ion-row >\n\n        <ion-col col-6>\n            <p class="pad">\n                <button ion-button color="secondary" outline class="button button-dark customBtn" (tap)="home()">Continue Order</button>\n              </p>\n\n        </ion-col>\n\n        <ion-col col-6>\n            <p class="pad">\n                <button ion-button color="secondary" class="button button-dark customBtn" (tap)="order_history()">Check Order History</button>\n              </p>\n\n        </ion-col>\n\n      </ion-row>\n    </div>\n\n   \n  </ion-footer>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/receipt/receipt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["b" /* CartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */]])
    ], ReceiptPage);
    return ReceiptPage;
}());

//# sourceMappingURL=receipt.js.map

/***/ })

});
//# sourceMappingURL=7.js.map