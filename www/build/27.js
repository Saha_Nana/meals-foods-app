webpackJsonp([27],{

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodDetailsPageModule", function() { return FoodDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_details__ = __webpack_require__(342);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodDetailsPageModule = /** @class */ (function () {
    function FoodDetailsPageModule() {
    }
    FoodDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_details__["a" /* FoodDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_details__["a" /* FoodDetailsPage */]),
            ],
        })
    ], FoodDetailsPageModule);
    return FoodDetailsPageModule;
}());

//# sourceMappingURL=food-details.module.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FoodDetailsPage = /** @class */ (function () {
    function FoodDetailsPage(apis, cartServ, loadingCtrl, alertCtrl, http, navCtrl, navParams) {
        var _this = this;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fakeUsers = new Array(5);
        console.log("--------------CONSTRUCTOR START---------------------");
        this.user_details = this.navParams.get("user_details");
        this.food_id = this.navParams.get("food_id");
        this.food_id_jsonBody = {
            "food_id": this.food_id
        };
        console.log('food_id ID retrieved:', this.food_id);
        this.apis.food_details(this.food_id_jsonBody).then(function (result) {
            _this.body = result;
            _this.foods_details = JSON.stringify(_this.body);
            console.log("Lets see all the foods_details " + _this.foods_details);
            _this.old_price = _this.body[0].price;
            console.log(_this.old_price);
        });
        console.log("-----------------------------------");
        this.count = this.cartServ.getAllCartfoodItems().length;
        console.log("FIRST COUNT " + this.count);
        console.log("--------------CONSTRUCTOR END---------------------");
    }
    FoodDetailsPage.prototype.quantityAdd = function (item) {
        console.log("--------------ITEM PRICE---------------------");
        this.cartServ.quantityPlus(item);
        console.log(item);
    };
    FoodDetailsPage.prototype.quantityMinus = function (item) {
        console.log("--------------MINUM PRICE: " + this.old_price);
        console.log("--------------ITEM PRICE: " + item.price);
        if (item.price > this.old_price) {
            console.log("--------------MINUM PRICE: " + this.old_price);
            console.log("--------------ITEM PRICE GREATER: " + item.price);
            this.cartServ.quantityMinus(item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Amount cannot be less than the minimum price.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    FoodDetailsPage.prototype.addTocart = function (item) {
        console.log("These are the items " + item);
        var pro = JSON.stringify(item);
        console.log("NOw strign " + pro);
        console.log("QUantity " + item.quantity);
        console.log("ITEM PRICE " + item.price);
        //ADDING NEW PRICE TO THE ITEM
        this.typed_price = parseFloat(this.price).toFixed(2);
        //this.typed_price =  this.price
        console.log(this.typed_price);
        var food_details = item;
        if (this.typed_price !== 'NaN') {
            console.log("TYPED IN PRICE" + this.price);
            item.price = parseFloat(parseFloat(this.price).toFixed(2));
            //item.price = parseInt(this.price)
            // console.log("NEW PRICE array " + food_details.new_food_price);
            console.log("NEW PRICE array " + food_details);
            console.log("NEW PRICE stringify " + JSON.stringify(food_details));
            console.log("item.price " + item.price);
        }
        //item.price =  parseFloat(item.price).toFixed(2);
        this.cartServ.addItem(food_details, item.quantity);
        console.log(this.cartServ.getAllCartfoodItems());
        this.count = this.cartServ.getAllCartfoodItems().length;
        console.log("WHAT IS COUNT " + this.count);
        this.navCtrl.push("FoodMenuPage", { user_details: this.user_details });
    };
    FoodDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-details',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-details/food-details.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title text-wrap>Add to cart</ion-title>\n\n\n    <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="cart()">\n\n        <!-- <img src="assets/imgs/cart.png" height="30px" width="30px" class="right" /> -->\n        <ion-icon name="cart" class="right" ></ion-icon>\n        <ion-badge>{{count}}</ion-badge>\n\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="background" >\n<ion-list *ngIf="body">\n<ion-list *ngFor="let item of body">\n  <ion-item class="center">\n   \n    <img [src]="item.food_logo_url" height="100px" width="150px"/>\n  </ion-item>\n\n  <ion-item>\n    <ion-list>\n      <h2>\n        <b style="color: black">{{item.food_name}}</b>\n      </h2>\n      <p>{{item.description}} </p>\n      <!-- <p> Mininum Price Order: Ghc {{item.price}} </p> -->\n    </ion-list>\n\n  </ion-item>\n\n \n\n\n    <ion-item no-lines>\n      <ion-list>\n          <p>\n            <b style="color: black">Determine how much you want to buy (GHC)</b>\n          </p>\n        \n        </ion-list>\n    <ion-row>\n\n      <ion-col col-4 tappable>\n        \n        <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger" (tap)="quantityMinus(item)"></ion-icon>\n      </ion-col>\n\n      <ion-col col-4>\n        <h4 class="margin">\n          \n            {{ item.price | number:\'1.2-2\'}}\n        </h4>\n      </ion-col>\n      <ion-col col-4 tappable>\n          <ion-icon ios="ios-add-circle" md="md-add-circle" color="secondary" (tap)="quantityAdd(item)"></ion-icon>\n      </ion-col>\n\n    </ion-row>\n\n    \n    <p class="middle">OR</p>\n       \n  \n    <ion-item no-lines>\n      \n      <p>\n          <b style="color: black">Enter how much you want to buy (GHC)</b>\n        </p>\n  \n    </ion-item>\n    <ion-item>\n      <ion-input type="number" placeholder="Enter amount here..." name="price" [(ngModel)]="price"></ion-input>\n    </ion-item> \n\n \n  </ion-item>\n\n\n\n\n  <ion-item no-lines>\n    <h2>Special Request (optional)</h2>\n\n  </ion-item>\n  <ion-item>\n    <ion-input type="text" placeholder="Type here..."></ion-input>\n  </ion-item>\n\n\n  <!-- <ion-footer> -->\n    <p>\n      <button ion-button color="secondary" class="button button-dark customBtn" (tap)="addTocart(item)"> Add To Cart</button>\n    </p>\n  <!-- </ion-footer> -->\n</ion-list>\n</ion-list>\n\n\n\n<ion-list *ngIf="!body">\n\n  <ion-list-header *ngFor="let fake of fakeUsers; let i = index" no-lines no-padding class="fakeItem">\n    <ion-list no-lines>\n\n      <ion-item class="center">\n   \n        <img [src]=""/>\n      </ion-item>\n    \n      <ion-item>\n        <ion-list>\n          <h2>\n            <b style="color: black"></b>\n          </h2>\n          <p></p>\n          <p> </p>\n        </ion-list>   \n      </ion-item>\n \n\n    </ion-list>\n  </ion-list-header>\n</ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-details/food-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], FoodDetailsPage);
    return FoodDetailsPage;
}());

//# sourceMappingURL=food-details.js.map

/***/ })

});
//# sourceMappingURL=27.js.map