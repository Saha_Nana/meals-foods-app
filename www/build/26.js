webpackJsonp([26],{

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodMenuPageModule", function() { return FoodMenuPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_menu__ = __webpack_require__(343);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodMenuPageModule = /** @class */ (function () {
    function FoodMenuPageModule() {
    }
    FoodMenuPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_menu__["a" /* FoodMenuPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_menu__["a" /* FoodMenuPage */]),
            ],
        })
    ], FoodMenuPageModule);
    return FoodMenuPageModule;
}());

//# sourceMappingURL=food-menu.module.js.map

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FoodMenuPage = /** @class */ (function () {
    function FoodMenuPage(alertCtrl, _form, apis, cartServ, loadingCtrl, http, navCtrl, navParams) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this._form = _form;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fakeUsers = new Array(5);
        this.catCode = [];
        this.user_details = this.navParams.get("user_details");
        // let localData = http.get('assets/foods.json').map(res => res.json().items);
        // localData.subscribe(data => {
        //   this.information = data;
        //   console.log("Information", this.information )
        // })  
        this.amountForm = this._form.group({
            "price": [""],
        });
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.fetch_foodstuff_menus().then(function (result) {
            _this.body = result;
            console.log(_this.body);
            console.log(JSON.stringify(_this.body));
            console.log("Information", _this.body);
            console.log("-------------------------------------------------------");
        });
        console.log("-----------------------------------");
        this.count = this.cartServ.getAllCartfoodItems().length;
        console.log("FIRST COUNT " + this.count);
        loader.dismiss();
    }
    FoodMenuPage.prototype.loadBody = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.fetch_foodstuff_menus().then(function (result) {
            _this.body = result;
            console.log(_this.body);
            console.log(JSON.stringify(_this.body));
            console.log("Information", _this.body);
            console.log("-------------------------------------------------------");
        });
        console.log("-----------------------------------");
        this.count = this.cartServ.getAllCartfoodItems().length;
        console.log("FIRST COUNT " + this.count);
        loader.dismiss();
    };
    FoodMenuPage.prototype.filterJson = function (ev) {
        var val = ev.target.value;
        if (val && val.trim() != '') {
            console.log("In the filter");
            this.body = this.body.filter(function (item) {
                console.log(item);
                for (var x in item) {
                    console.log(x);
                    console.log(item[x]);
                }
                return (item.food_menu.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            console.log("Outside the filter");
            this.loadBody();
        }
    };
    FoodMenuPage.prototype.details = function (food_item) {
        console.log("Lets see the foods id " + food_item.food_id);
        this.navCtrl.push("FoodDetailsPage", { food_id: food_item.food_id, user_details: this.user_details });
    };
    FoodMenuPage.prototype.toggleSection = function (i) {
        console.log("Lets see I", i);
        this.body[i].open = !this.body[i].open;
    };
    FoodMenuPage.prototype.toggleItem = function (i, j) {
        console.log("Lets see I J", i, j);
        this.body[i].children[j].open = !this.body[i].children[j].open;
    };
    FoodMenuPage.prototype.cart = function () {
        this.navCtrl.push("FoodCartPage", { user_details: this.user_details });
    };
    FoodMenuPage.prototype.quantityAdd = function (food_item) {
        console.log("-------QUATITY " + food_item.quantity);
        this.cartServ.quantityPlus(food_item);
    };
    FoodMenuPage.prototype.quantityMinus = function (food_item) {
        if (food_item.quantity > 1) {
            this.cartServ.quantityMinus(food_item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Alert!',
                subTitle: 'Quantity is 1, you cant reduce it.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    FoodMenuPage.prototype.addTocart = function (food_item) {
        // console.log ("LETS SEE CATCODE PRICE " + this.catCode)
        this.priceVal = JSON.stringify(this.amountForm.value);
        console.log("LETS SEE THE LOGIN VAL " + this.priceVal);
        this.jsonBody = JSON.parse(this.priceVal);
        console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody);
        console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody["price"]);
        this.price = this.jsonBody["price"];
        console.log("PRICE FROM FORM" + this.jsonBody["price"]);
        console.log("These are the items " + food_item);
        var pro = JSON.stringify(food_item);
        console.log("NOw strign " + pro);
        console.log("QUantity " + food_item.quantity);
        console.log("ITEM PRICE " + food_item.price);
        //ADDING NEW PRICE TO THE ITEM
        this.typed_price = parseFloat(this.price).toFixed(2);
        //this.typed_price =  this.price
        console.log(this.typed_price);
        var food_details = food_item;
        if (this.typed_price !== 'NaN') {
            console.log("TYPED IN PRICE" + this.price);
            food_item.price = parseFloat(parseFloat(this.price).toFixed(2));
            //item.price = parseInt(this.price)
            // console.log("NEW PRICE array " + food_details.new_food_price);
            console.log("NEW PRICE array " + food_details);
            console.log("NEW PRICE stringify " + JSON.stringify(food_details));
            console.log("item.price " + food_item.price);
        }
        if (food_item.quantity > '0') {
            console.log("food_item.price" + food_item.price);
            food_item.price = food_item.price * food_item.quantity;
            //item.price = parseInt(this.price)
            // console.log("NEW PRICE array " + food_details.new_food_price);
            console.log("NEW PRICE array " + food_details);
            console.log("NEW PRICE stringify " + JSON.stringify(food_details));
            console.log("item.price " + food_item.price);
        }
        //item.price =  parseFloat(item.price).toFixed(2);
        this.cartServ.addItem(food_details, food_item.quantity);
        console.log(this.cartServ.getAllCartfoodItems());
        this.count = this.cartServ.getAllCartfoodItems().length;
        console.log("WHAT IS COUNT " + this.count);
        // this.navCtrl.push("FoodMenuPage",{ user_details: this.user_details})
        this.amountForm.reset();
    };
    FoodMenuPage.prototype.lists = function () {
        this.navCtrl.push("ListPage", { customer_details: this.user_details });
    };
    FoodMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-menu',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-menu/food-menu.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>List Of Foodstuffs</ion-title>\n    <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="lists()">\n        <ion-icon name="add" class="right"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="cart()">\n        <!-- <img src="assets/imgs/cart.png" height="30px" width="30px" class="right" /> -->\n        <ion-icon name="cart" class="right"></ion-icon>\n        <ion-badge>{{count}}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-searchbar\n    (ionInput)="filterJson($event)"\n    placeholder="Search..."\n  ></ion-searchbar>\n\n  <ion-list no-lines *ngIf="body">\n    <ion-list class="accordion-list" overflow-scroll="true">\n      <ion-list-header\n        *ngFor="let item of body; let i = index"\n        no-lines\n        no-padding\n      >\n        <button\n          ion-item\n          (click)="toggleSection(i)"\n          detail-none\n          [ngClass]="{\'section-active\': item.open, \'section\': !item.open}"\n        >\n          <ion-icon item-right name="arrow-down" *ngIf="!item.open"></ion-icon>\n          <ion-icon item-right name="arrow-up" *ngIf="item.open"></ion-icon>\n          {{ item.food_menu }}\n        </button>\n\n        <ion-list *ngIf="item.open" no-lines>\n          <ion-list-header\n            *ngFor="let food_item of item.foods; let j = index"\n            no-padding\n          >\n            <ion-row class="container">\n              <ion-col col-2>\n                <ion-avatar item-start>\n                  <img [src]="food_item.food_logo_url" />\n                </ion-avatar>\n              </ion-col>\n\n              <ion-col col-8>\n                <h2 text-wrap>\n                  <b style="color: black" text-wrap>{{food_item.food_name}}</b>\n                </h2>\n\n                <p text-wrap>\n                  Minimum Price: GHC{{ food_item.price | number:\'1.2-2\'}}\n                </p>\n\n                <form [formGroup]="amountForm" *ngIf="food_item.quantity == 0">\n                  <ion-item no-lines class="movemove">\n                    <!-- <ion-input type="number" class="movemove" placeholder="Enter amount here..." name="price" [(ngModel)]="catCode[j]"></ion-input><br> -->\n\n                    <ion-input\n                      type="number"\n                      class="movemove"\n                      placeholder="Enter amount here..."\n                      name="price"\n                      formControlName="price"\n                      [class.invalid]="!amountForm.controls.price.valid && (amountForm.controls.price.dirty || submitAttempt)"\n                    ></ion-input>\n                  </ion-item>\n                </form>\n\n                <ion-list *ngIf="food_item.quantity > 0">\n                  <b text-wrap style="color: #781318"\n                    >Determine quantities you want\n                  </b>\n                  <ion-row>\n                    <ion-col col-4 tappable>\n                      <ion-icon\n                        ios="ios-remove-circle"\n                        md="md-remove-circle"\n                        color="danger"\n                        tappable\n                        (tap)="quantityMinus(food_item)"\n                      ></ion-icon>\n                    </ion-col>\n\n                    <ion-col col-4>\n                      <h4 class="margin">{{ food_item.quantity}}</h4>\n                    </ion-col>\n                    <ion-col col-4 tappable>\n                      <ion-icon\n                        ios="ios-add-circle"\n                        md="md-add-circle"\n                        color="secondary"\n                        tappable\n                        (tap)="quantityAdd(food_item)"\n                      ></ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-list>\n              </ion-col>\n\n              <ion-col col-2>\n                <ion-icon\n                  name="add-circle"\n                  tappable\n                  class="move-center"\n                  (tap)="addTocart(food_item)"\n                ></ion-icon>\n                <!-- <ion-icon name="add-circle" tappable class="move-center" (tap)="details(food_item)"></ion-icon> -->\n              </ion-col>\n            </ion-row>\n          </ion-list-header>\n        </ion-list>\n      </ion-list-header>\n    </ion-list>\n  </ion-list>\n\n  <ion-list *ngIf="!body">\n    <ion-list-header\n      *ngFor="let fake of fakeUsers; let i = index"\n      no-lines\n      no-padding\n      class="fakeItem"\n    >\n      <ion-list no-lines>\n        <ion-row class="container">\n          <ion-col col-2>\n            <ion-avatar item-start>\n              <img [src]="" />\n            </ion-avatar>\n          </ion-col>\n\n          <ion-col col-7>\n            <h2>\n              <b></b>\n            </h2>\n            <p></p>\n          </ion-col>\n\n          <ion-col col-3>\n            <h2></h2>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n    </ion-list-header>\n  </ion-list>\n</ion-content>\n\n<ion-footer *ngIf="count > 0">\n  <p>\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      (tap)="cart()"\n    >\n      View Cart ({{count}})\n    </button>\n  </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-menu/food-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], FoodMenuPage);
    return FoodMenuPage;
}());

//# sourceMappingURL=food-menu.js.map

/***/ })

});
//# sourceMappingURL=26.js.map