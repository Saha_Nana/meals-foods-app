webpackJsonp([13],{

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function() { return OrderDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_details__ = __webpack_require__(356);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OrderDetailsPageModule = /** @class */ (function () {
    function OrderDetailsPageModule() {
    }
    OrderDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */]),
            ],
        })
    ], OrderDetailsPageModule);
    return OrderDetailsPageModule;
}());

//# sourceMappingURL=order-details.module.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderDetailsPage = /** @class */ (function () {
    function OrderDetailsPage(navCtrl, navParams, apis, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.orderJson = this.navParams.get('value');
        this.user_details = this.navParams.get("customer_details");
        console.log("ORDER ID IN MODAL " + this.orderJson);
        this.params = {
            "order_id": this.orderJson
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.order_details(this.params).then(function (result) {
            // var body1 = result["_body"];
            // body1 = JSON.parse(body1);
            _this.list = Array.of(result);
            _this.orderJson = JSON.stringify(result);
            _this.order_id = result['order_id'];
            console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
            console.log('LETS SEE THE ORDER ID IN PROCESS ORDER ' + _this.order_id);
            loader.dismiss();
        });
    }
    OrderDetailsPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    OrderDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order-details',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/order-details/order-details.html"*/'<ion-header>\n    <ion-navbar>\n  \n      <ion-title> <b> Order Details </b> </ion-title>\n    </ion-navbar>\n  </ion-header>\n\n\n<ion-content >\n    \n  \n     <!-- <ion-card-content>\n        <ion-card> -->\n  \n      <ion-list *ngFor="let q of list">\n  \n        <ion-list *ngFor=" let option of q.order_details">\n  \n          <ion-item no-lines>\n            <ion-thumbnail item-left>\n           <img *ngIf="option" [src]="option.meal_logo_url"  />\n            </ion-thumbnail>\n            <h4 text-wrap> Product name: {{ option.meals_name}}</h4>\n            <h4 text-wrap> Price: GHC {{ option.price}}</h4>\n            <h4 text-wrap>Quantity: {{ option.quantity}}</h4>\n          </ion-item>\n  \n        </ion-list>\n      </ion-list>\n      <br>\n      <!-- </ion-card>\n  \n      </ion-card-content> -->\n  </ion-content>\n\n  <ion-footer >\n      <p>\n        <button ion-button color="dark" class="button button-dark customBtn" (tap)="home()"> Order Again? </button>\n      </p>\n    \n    </ion-footer>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/order-details/order-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OrderDetailsPage);
    return OrderDetailsPage;
}());

//# sourceMappingURL=order-details.js.map

/***/ })

});
//# sourceMappingURL=13.js.map