webpackJsonp([30],{

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackPageModule", function() { return FeedbackPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feedback__ = __webpack_require__(339);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FeedbackPageModule = /** @class */ (function () {
    function FeedbackPageModule() {
    }
    FeedbackPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__feedback__["a" /* FeedbackPage */]),
            ],
        })
    ], FeedbackPageModule);
    return FeedbackPageModule;
}());

//# sourceMappingURL=feedback.module.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FeedbackPage = /** @class */ (function () {
    function FeedbackPage(app, actionSheetCtrl, platform, navCtrl, apis, _form, toastCtrl, navParams, http, loadingCtrl, alertCtrl) {
        this.app = app;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.apis = apis;
        this._form = _form;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.feedbackForm = this._form.group({
            "subject": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "message": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
        this.from_login = this.navParams.get("customer_details");
        //Checking if user data is shown / logged in
        if (this.from_login == undefined) {
            console.log("No Login - NO User Data");
        }
        else {
            this.user_details = JSON.parse(this.from_login);
            console.log("this.user_details " + JSON.stringify(this.user_details));
            this.jsonBody = this.user_details; // this.jsonBody = JSON.parse(this.body);
            this.first_name = this.jsonBody[0].first_name;
            this.last_name = this.jsonBody[0].last_name;
            this.user_id = this.jsonBody[0].id;
            console.log("first_name" + this.first_name);
        }
    }
    FeedbackPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedbackPage');
    };
    FeedbackPage.prototype.send_feedback = function () {
        var _this = this;
        this.feedbackVal = JSON.stringify(this.feedbackForm.value);
        this.jsonBody = JSON.parse(this.feedbackVal);
        console.log("THIS IS THE Feedback raw values VALUES" + this.feedbackVal);
        console.log("THIS IS THE Feedback VALUES " + this.jsonBody);
        this.params = {
            "user_id": this.user_id,
            "requester_name": this.first_name + " " + this.last_name,
            "subject": this.jsonBody.subject,
            "message": this.jsonBody.message,
        };
        console.log("this.params = " + JSON.stringify(this.params));
        var loader = this.loadingCtrl.create({
            content: "Please wait ..."
        });
        loader.present();
        this.apis.send_feedback(this.params).then(function (result) {
            console.log("THIS IS THE RESULT" + result);
            console.log("result" + JSON.stringify(result));
            var resp_code = result["resp_code"];
            var resp_desc = result["resp_desc"];
            console.log(resp_code);
            console.log(resp_desc);
            _this.messageList = resp_desc;
            _this.api_code = resp_code;
            loader.dismiss();
            if (_this.api_code == "000") {
                var alert_1 = _this.alertCtrl.create({
                    title: "Feedback To M&F",
                    subTitle: _this.messageList,
                    buttons: [
                        {
                            text: 'OK', handler: function () {
                                _this.navCtrl.setRoot("HomePage", { "customer_details": _this.from_login });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            if (_this.api_code != "000") {
                _this.showmessage(_this.messageList);
            }
        }, function (err) {
            loader.dismiss();
            _this.toastCtrl.create({
                message: "Could not process this request successfully.",
                duration: 5000
            }).present();
            console.log(err);
        });
    };
    FeedbackPage.prototype.showmessage = function (message) {
        var alert = this.alertCtrl.create({
            title: "Feeback",
            subTitle: message,
            buttons: [
                {
                    text: 'OK', handler: function () { }
                }
            ]
        });
        alert.present();
    };
    FeedbackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-feedback',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/feedback/feedback.html"*/'\n\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Feedback</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content overflow-scroll=\'true\' class="card">\n    <div class=""   >\n  <p><b class="fontt">Hello, {{first_name}} {{last_name}} </b> </p>\n  <!-- <p><b style=" color: #5dba47; text-align: center"></b> </p> -->\n\n  <br />\n  <form [formGroup]="feedbackForm">\n\n    <br />\n    <ion-list>\n        <ion-item >\n            <ion-label id="push_label" style=" color: #781318; text-align: center">\n                Your Feedback is appreciated\n               </ion-label>\n        </ion-item >\n      <ion-item >\n          <!-- <ion-label id="push_label">\n              Appointment\n             </ion-label> -->\n        \n              <ion-label stacked>Subject</ion-label>\n              <ion-input type="text" placeholder="" formControlName="subject" name="subject"  [class.invalid]="!feedbackForm.controls.subject.valid && (feedbackForm.controls.subject.dirty || submitAttempt)"></ion-input>\n    \n      </ion-item> \n\n   \n\n      \n          \n        <ion-item>\n            <!-- <ion-label stacked>Enter your message here</ion-label>\n          <ion-textarea rows="4" placeholder="" formControlName="message" name="message"\n            [class.invalid]="!feedbackForm.controls.message.valid && (feedbackForm.controls.message.dirty || submitAttempt)"></ion-textarea> -->\n        \n            <ion-label stacked>Enter your message here</ion-label>\n              <ion-input type="text" rows="4" placeholder="" formControlName="message" name="message"  [class.invalid]="!feedbackForm.controls.message.valid && (feedbackForm.controls.message.dirty || submitAttempt)"></ion-input>\n    \n        \n        \n          </ion-item>\n      \n\n     \n\n\n    \n\n      <p class="pad" style="text-align: center;">\n        <button ion-button color="green1" class="submitbtn" [disabled]="!this.feedbackForm.valid" (tap)="send_feedback()"\n          style="text-align:center;border-radius: 60px; height: 40px; width: 90%"> Submit\n        </button>\n      </p>\n\n    </ion-list>\n\n\n\n\n\n  </form>\n</div>\n</ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/feedback/feedback.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FeedbackPage);
    return FeedbackPage;
}());

//# sourceMappingURL=feedback.js.map

/***/ })

});
//# sourceMappingURL=30.js.map