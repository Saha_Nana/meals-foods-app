webpackJsonp([38],{

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CateringMenuPageModule", function() { return CateringMenuPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__catering_menu__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CateringMenuPageModule = /** @class */ (function () {
    function CateringMenuPageModule() {
    }
    CateringMenuPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__catering_menu__["a" /* CateringMenuPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__catering_menu__["a" /* CateringMenuPage */]),
            ],
        })
    ], CateringMenuPageModule);
    return CateringMenuPageModule;
}());

//# sourceMappingURL=catering-menu.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CateringMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CateringMenuPage = /** @class */ (function () {
    function CateringMenuPage(apis, cartServ, loadingCtrl, http, navCtrl, navParams, alertCtrl) {
        var _this = this;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.food_cat = "breakfast";
        this.fakeUsers = new Array(5);
        this.user_details = this.navParams.get("user_details");
        this.restaurant_id = this.navParams.get("restaurant_id");
        console.log('restaurant_id ID retrieved:', this.restaurant_id);
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.fetch_catering_menus(this.restaurant_id).then(function (result) {
            _this.body = result;
            console.log("body from menu", _this.body);
            if (_this.body != null) {
                _this.body2 = result[0].meals;
                console.log("body2", _this.body2);
                _this.menu_details = JSON.stringify(_this.body);
                _this.restaurant_details = JSON.stringify(_this.body2);
                _this.restaurant_name = _this.body2[0].restaurant_name;
                _this.restaurant_logo = _this.body2[0].logo;
                _this.restaurant_description = _this.body2[0].descriptions;
                console.log("-------------------------------------------------------");
                console.log(_this.body);
                console.log(JSON.stringify(_this.body));
                console.log("restaurant_name", _this.restaurant_name);
                console.log("restaurant_logo", _this.restaurant_logo);
                console.log("restaurant_description", _this.restaurant_description);
                console.log("-------------------------------------------------------");
            }
            else {
                _this.body == 0;
                console.log("bodty is 0000");
            }
        });
        console.log("-----------------------------------");
        this.count = this.cartServ.getAllCateringCartItems().length;
        console.log("FIRST COUNT " + this.count);
        loader.dismiss();
    }
    CateringMenuPage.prototype.details = function (item) {
        console.log("Lets see the meals id " + item.menu_items_id);
        this.navCtrl.push("CateringItemDetailsPage", { meals_id: item.menu_items_id, restaurant_id: this.restaurant_id, user_details: this.user_details });
    };
    CateringMenuPage.prototype.toggleSection = function (i) {
        console.log("Lets see Index", i);
        this.body[i].open = !this.body[i].open;
    };
    CateringMenuPage.prototype.toggleItem = function (i, j) {
        console.log("Lets see I J", j);
        // this.information[i].children[j].open = !this.information[i].children[j].open;
        this.body[i].meals[j].open = !this.body[i].meals[j].open;
    };
    CateringMenuPage.prototype.cart = function () {
        this.navCtrl.push("CateringCartPage", { user_details: this.user_details });
    };
    CateringMenuPage.prototype.addTocart = function (meal_item) {
        console.log("These are the items " + meal_item);
        console.log("MEAL QUANTITYY " + meal_item.quantity);
        var pro = JSON.stringify(meal_item);
        console.log("NOw strign " + pro);
        this.cartServ.addItem(meal_item, meal_item.quantity);
        console.log("ADD CART" + this.cartServ.addItem(meal_item, meal_item.quantity));
        console.log("LETS SEE ALL CART ITEM" + this.cartServ.getAllCateringCartItems());
        console.log(this.cartServ.getAllCateringCartItems());
        this.count = this.cartServ.getAllCateringCartItems().length;
        console.log("WHAT IS COUNT " + this.count);
        var string = JSON.stringify(this.count);
        console.log("STRINGFIY " + string);
    };
    CateringMenuPage.prototype.quantityAdd = function (item) {
        this.cartServ.quantityPlus(item);
    };
    CateringMenuPage.prototype.quantityMinus = function (item) {
        if (item.quantity > 1) {
            this.cartServ.quantityMinus(item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Quantity is 1, you cant reduce it.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    CateringMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-catering-menu',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/catering-menu/catering-menu.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ restaurant_name}}</ion-title>\n\n    <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="cart()">\n        <!-- <img src="assets/imgs/cart.png" height="30px" width="30px" class="right" /> -->\n        <ion-icon name="cart" class="right"></ion-icon>\n        <ion-badge>{{count}}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content overflow-scroll="true" class="background">\n  <!-- <div class="center1" *ngIf="body?.length == 0 || body?.length == null" text-wrap>\n    <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n    <p style="color: #757575; text-align: center"> Menu not available for this restaurant yet</p>\n  </div> -->\n\n  <ion-list *ngIf="body?.length > 0">\n    <ion-list-header>\n      <ion-row>\n        <ion-col col-3>\n          <ion-avatar item-start>\n            <img [src]="restaurant_logo" />\n          </ion-avatar>\n        </ion-col>\n        <ion-col col-9>\n          <h2>\n            <b style="color: black" text-wrap>{{ restaurant_name}}</b>\n          </h2>\n          <p text-wrap>{{ restaurant_description}}</p>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star" class="star"></ion-icon>\n          <ion-icon name="star-half" class="star"></ion-icon>\n          <br />\n        </ion-col>\n      </ion-row>\n    </ion-list-header>\n\n    <ion-segment [(ngModel)]="food_cat" overflow-scroll="true">\n      <ion-segment-button class="segment-button" value="breakfast">\n        Menu\n      </ion-segment-button>\n      <!-- <ion-segment-button value="lunch"> Reviews </ion-segment-button>\n      <ion-segment-button value="dinner"> Info </ion-segment-button> -->\n    </ion-segment>\n\n    <div [ngSwitch]="food_cat">\n      <ion-list *ngSwitchCase="\'breakfast\'" overflow-scroll="true">\n        <ion-list class="accordion-list" overflow-scroll="true">\n          <ion-list-header\n            *ngFor="let item of body; let i = index"\n            no-lines\n            no-padding\n          >\n            <button\n              ion-item\n              (tap)="toggleSection(i)"\n              detail-none\n              [ngClass]="{\'section-active\': item.open, \'section\': !item.open}"\n              text-wrap\n            >\n              <ion-icon\n                item-right\n                name="arrow-down"\n                *ngIf="!item.open"\n              ></ion-icon>\n              <ion-icon item-right name="arrow-up" *ngIf="item.open"></ion-icon>\n              {{ item.menu_name }}\n            </button>\n\n            <ion-list *ngIf="item.meals && item.open" no-lines>\n              <ion-list-header\n                *ngFor="let meal_item of item.meals; let j = index"\n                no-padding\n              >\n                <!--- if meals has children-->\n                <button\n                  ion-item\n                  (tap)="toggleSection(i,j)"\n                  detail-none\n                  *ngIf="meal_item.children"\n                  text-wrap\n                >\n                  <ion-icon\n                    item-right\n                    name="add"\n                    *ngIf="!meal_item.open"\n                  ></ion-icon>\n                  <ion-icon\n                    item-right\n                    name="close"\n                    *ngIf="meal_item.open"\n                  ></ion-icon>\n                  {{ item.menu_name }}\n                </button>\n\n                <!--- if meals has no children then display the values-->\n\n                <ion-row class="container" *ngIf="!meal_item.children">\n                  <ion-col col-2 tappable>\n                    <ion-avatar item-start>\n                      <img [src]="meal_item.meal_logo_url" />\n                    </ion-avatar>\n                  </ion-col>\n\n                  <ion-col col-7 text-wrap>\n                    <h2 tappable>\n                      <b style="color: black">{{meal_item.meal_name}}</b>\n                    </h2>\n                    <p text-wrap tappable>{{meal_item.description}}</p>\n\n                    <p text-wrap tappable>Minimum Order Price: GHC100.00</p>\n\n                    <ion-row text-wrap>\n                      <!-- \n                      <ion-col col-4>\n\n                        <p text-wrap class="margin">Quantity: </p>\n                      </ion-col> -->\n                      <ion-col col-2 tappable>\n                        <ion-icon\n                          ios="ios-remove-circle"\n                          md="md-remove-circle"\n                          color="danger"\n                          (tap)="quantityMinus(meal_item)"\n                        ></ion-icon>\n                      </ion-col>\n\n                      <ion-col col-2>\n                        <h4 class="margin">{{meal_item.quantity}}</h4>\n                      </ion-col>\n                      <ion-col col-2 tappable>\n                        <ion-icon\n                          ios="ios-add-circle"\n                          md="md-add-circle"\n                          color="fats"\n                          (tap)="quantityAdd(meal_item)"\n                        ></ion-icon>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n\n                  <ion-col col-3 text-wrap>\n                    <h2>Ghc {{meal_item.price}}</h2>\n                    <!-- <ion-icon name="add-circle" tappable class="move-center" (tap)="addTocart(meal_item)"></ion-icon> -->\n\n                    <p>\n                      <button\n                        ion-button\n                        color="secondary"\n                        outline\n                        (tap)="addTocart(meal_item)"\n                      >\n                        Add\n                      </button>\n                    </p>\n                  </ion-col>\n                  <ion-item>\n                    <ion-textarea\n                      placeholder="Tell us what more you want here"\n                      auto-grow="true"\n                      rows="4"\n                    ></ion-textarea>\n                  </ion-item>\n                </ion-row>\n              </ion-list-header>\n            </ion-list>\n          </ion-list-header>\n        </ion-list>\n      </ion-list>\n\n      <ion-list *ngSwitchCase="\'lunch\'"> </ion-list>\n\n      <ion-list *ngSwitchCase="\'dinner\'"> </ion-list>\n    </div>\n  </ion-list>\n\n  <ion-list *ngIf="!body">\n    <div [ngSwitch]="food_cat">\n      <ion-list *ngSwitchCase="\'breakfast\'">\n        <ion-list class="accordion-list">\n          <ion-list-header\n            *ngFor="let fake of fakeUsers; let i = index"\n            no-lines\n            no-padding\n            class="fakeItem"\n          >\n            <ion-list no-lines>\n              <ion-row class="container">\n                <ion-col col-2 tappable>\n                  <ion-avatar item-start>\n                    <img [src]="" />\n                  </ion-avatar>\n                </ion-col>\n\n                <ion-col col-7 tappable>\n                  <h2>\n                    <b></b>\n                  </h2>\n                  <p></p>\n                </ion-col>\n\n                <ion-col col-3>\n                  <h2></h2>\n                </ion-col>\n              </ion-row>\n            </ion-list>\n          </ion-list-header>\n        </ion-list>\n      </ion-list>\n    </div>\n  </ion-list>\n</ion-content>\n\n<ion-footer *ngIf="count > 0">\n  <p>\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      (tap)="cart()"\n    >\n      View Cart ({{count}})\n    </button>\n  </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/catering-menu/catering-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["c" /* CateringCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], CateringMenuPage);
    return CateringMenuPage;
}());

//# sourceMappingURL=catering-menu.js.map

/***/ })

});
//# sourceMappingURL=38.js.map