webpackJsonp([3],{

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchcateringPageModule", function() { return SearchcateringPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__searchcatering__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SearchcateringPageModule = /** @class */ (function () {
    function SearchcateringPageModule() {
    }
    SearchcateringPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__searchcatering__["a" /* SearchcateringPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__searchcatering__["a" /* SearchcateringPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__["a" /* IonicSelectableModule */]
            ],
        })
    ], SearchcateringPageModule);
    return SearchcateringPageModule;
}());

//# sourceMappingURL=searchcatering.module.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchcateringPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SearchcateringPage = /** @class */ (function () {
    function SearchcateringPage(_form, apis, loadingCtrl, navCtrl, navParams) {
        var _this = this;
        this._form = _form;
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user_details = this.navParams.get("user_details");
        this.searchForm = this._form.group({
            "location_name": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])]
        });
        this.apis.retrieve_all_locations().then(function (result) {
            _this.body = result;
            _this.locations = _this.body;
        });
    }
    SearchcateringPage.prototype.locationChange = function (event) {
        this.location = event.value;
        console.log('Location Id:', this.location.id);
        console.log('Location Name:', this.location.location_name);
    };
    SearchcateringPage.prototype.restaurants = function () {
        this.navCtrl.push("CateringlistPage", { location_id_value: this.location.id, location_name: this.location.location_name, user_details: this.user_details });
    };
    SearchcateringPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-searchcatering',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/searchcatering/searchcatering.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Find Catering Services</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content overflow-scroll="true" class="background">\n  <div class="padding">\n    <b class="pad">\n      <!-- <img src="assets/imgs/mnf.png" width="50%" /> -->\n      <img src="assets/imgs/logo.png" width="50%" />\n    </b>\n  </div>\n\n  <p>\n    <b>Find Catering Services</b>\n  </p>\n  <form [formGroup]="searchForm">\n    <ion-item>\n      <ion-label>Location</ion-label>\n      <ionic-selectable\n        item-content\n        [(ngModel)]="location"\n        [items]="locations"\n        itemValueField="id"\n        itemTextField="location_name"\n        [canSearch]="true"\n        (onChange)="locationChange($event)"\n        formControlName="location_name"\n        name="location_name"\n        [class.invalid]="!searchForm.controls.location_name.valid && (searchForm.controls.location_name.dirty || submitAttempt)"\n      >\n      </ionic-selectable>\n    </ion-item>\n    <br />\n\n    <br />\n    <br />\n    <div class="">\n      <b class="pad">\n        <button\n          ion-button\n          class="button button-dark customBtn"\n          [disabled]="!this.searchForm.valid"\n          color="danger"\n          (tap)="restaurants()"\n        >\n          Find Caterers\n        </button>\n      </b>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/searchcatering/searchcatering.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], SearchcateringPage);
    return SearchcateringPage;
}());

//# sourceMappingURL=searchcatering.js.map

/***/ })

});
//# sourceMappingURL=3.js.map