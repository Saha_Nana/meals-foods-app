webpackJsonp([2],{

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(366);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, apis, _form, toastCtrl, navParams, http, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.apis = apis;
        this._form = _form;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.submitAttempt = false;
        this.from_cart_or_sidemenu = this.navParams.get("cart_or_sidemenu");
        console.log("From Sidemenu or cart " + this.from_cart_or_sidemenu);
        this.signupForm = this._form.group({
            "first_name": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "last_name": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "mobile_number": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "email": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "password": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "confirm_password": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
        });
    }
    SignupPage.prototype.signup = function () {
        var _this = this;
        this.signupVal = JSON.stringify(this.signupForm.value);
        this.jsonBody = JSON.parse(this.signupVal);
        console.log("THIS IS THE SIGNUP raw values VALUES" + this.signupVal);
        console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody);
        var loader = this.loadingCtrl.create({
            content: "Please wait ..."
        });
        loader.present();
        this.apis.signup(this.jsonBody).then(function (result) {
            console.log(result);
            var jsonBody = result;
            console.log(jsonBody);
            // jsonBody = JSON.parse(jsonBody);
            // console.log(jsonBody)
            var desc = jsonBody["resp_desc"];
            var code = jsonBody["resp_code"];
            console.log(desc);
            console.log(code);
            _this.messageList = desc;
            _this.api_code = code;
            loader.dismiss();
            if (_this.api_code == "200") {
                var loader_1 = _this.loadingCtrl.create({
                    content: "Verifying Account ..."
                });
                loader_1.present();
                setTimeout(function () {
                    if (_this.from_cart_or_sidemenu == "sidemenu") {
                        _this.navCtrl.setRoot("LoginPage", { cart_or_sidemenu: "sidemenu" });
                    }
                    else if (_this.from_cart_or_sidemenu == "cart") {
                        _this.navCtrl.setRoot("LoginPage", { cart_or_sidemenu: "cart" });
                    }
                    else if (_this.from_cart_or_sidemenu == "meals_cart") {
                        _this.navCtrl.setRoot("LoginPage", { cart_or_sidemenu: "meals_cart" });
                    }
                    else if (_this.from_cart_or_sidemenu == "catering_cart") {
                        _this.navCtrl.push("LoginPage", { cart_or_sidemenu: "catering_cart" });
                    }
                    else {
                        _this.navCtrl.setRoot("LoginPage", { cart_or_sidemenu: "sidemenu" });
                    }
                }, 3000);
                setTimeout(function () {
                    loader_1.dismiss();
                }, 3000);
            }
            if (_this.api_code != "200") {
                var alert_1 = _this.alertCtrl.create({
                    title: "",
                    subTitle: _this.messageList,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: "",
                subTitle: "Sorry, something went wrong. Please try again!",
                buttons: ['OK']
            });
            alert.present();
            _this.toastCtrl.create({
                message: "Please check your internet connection",
                duration: 5000
            }).present();
            loader.dismiss();
            console.log(err);
        });
    };
    SignupPage.prototype.login = function () {
        if (this.from_cart_or_sidemenu == "sidemenu") {
            this.navCtrl.push("LoginPage", { cart_or_sidemenu: "sidemenu" });
        }
        else if (this.from_cart_or_sidemenu == "cart") {
            this.navCtrl.push("LoginPage", { cart_or_sidemenu: "cart" });
        }
        else {
            this.navCtrl.push("LoginPage", { cart_or_sidemenu: "sidemenu" });
        }
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/signup/signup.html"*/'<ion-content overflow-scroll=\'true\' class="background">\n  <br/>\n  <br/>\n  <br/>\n  <!-- <br/>\n  <br/> -->\n  <form [formGroup]="signupForm" class="move_form">\n    <!--<ion-card class="main-view">-->\n\n\n    <ion-list class="main-view">\n      <br/>\n\n\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="first name" formControlName="first_name" name="first_name" [class.invalid]="!signupForm.controls.first_name.valid && (signupForm.controls.first_name.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-person" md="md-person"></ion-icon>\n        </ion-label>\n        <ion-label> </ion-label>\n      </ion-item>\n      <br/>\n      <ion-item>\n\n        <ion-input type="text" placeholder="Last name" formControlName="last_name" name="last_name" [class.invalid]="!signupForm.controls.last_name.valid && (signupForm.controls.last_name.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-person" md="md-person"></ion-icon>\n        </ion-label>\n        <ion-label> </ion-label>\n      </ion-item>\n      <br/>\n\n      <ion-item>\n        <ion-input type="number" placeholder="Mobile Number" formControlName="mobile_number" name="mobile_number" [class.invalid]="!signupForm.controls.mobile_number.valid && (signupForm.controls.mobile_number.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-phone-portrait" md="md-phone-portrait"></ion-icon>\n        </ion-label>\n        <ion-label> </ion-label>\n      </ion-item>\n      <br/>\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="Email" formControlName="email" name="email" [class.invalid]="!signupForm.controls.email.valid && (signupForm.controls.email.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon name="ios-mail"></ion-icon>\n        </ion-label>\n\n        <ion-label> </ion-label>\n      </ion-item>\n\n      <br/>\n      <ion-item>\n\n        <ion-input type="password" placeholder="Password" formControlName="password" name="password" [class.invalid]="!signupForm.controls.password.valid && (signupForm.controls.password.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-unlock" md="md-unlock"></ion-icon>\n        </ion-label>\n\n        <ion-label> </ion-label>\n      </ion-item>\n      <br/>\n      <ion-item>\n\n        <ion-input type="password" placeholder="confirm password" formControlName="confirm_password" name="confirm_password" [class.invalid]="!signupForm.controls.confirm_password.valid && (signupForm.controls.confirm_password.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-unlock" md="md-unlock"></ion-icon>\n        </ion-label>\n\n        <ion-label> </ion-label>\n      </ion-item>\n\n      <p>\n        <button ion-button color="kaya" class="button button-dark customBtn" [disabled]="!this.signupForm.valid" (tap)="signup()">\n          Sign up</button>\n      </p>\n\n\n    </ion-list>\n\n    <!-- <ion-footer> -->\n\n      <p class="pad">\n        <b style=" color: white;">\n          <p style=" color: white;">Already have an account ?\n            <u (tap)="login()" tappable style=" color: #30aeff;">Log in</u>\n          </p>\n        </b>\n      </p>\n\n    <!-- </ion-footer> -->\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=2.js.map