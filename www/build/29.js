webpackJsonp([29],{

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodCartPageModule", function() { return FoodCartPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_cart__ = __webpack_require__(340);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodCartPageModule = /** @class */ (function () {
    function FoodCartPageModule() {
    }
    FoodCartPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_cart__["a" /* FoodCartPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_cart__["a" /* FoodCartPage */]),
            ],
        })
    ], FoodCartPageModule);
    return FoodCartPageModule;
}());

//# sourceMappingURL=food-cart.module.js.map

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodCartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FoodCartPage = /** @class */ (function () {
    function FoodCartPage(navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.user_details = this.navParams.get("user_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
        }
        else {
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            console.log("USer DETAILS " + this.user_details);
            console.log("USer ID " + this.user_id);
            var loader = this.loadingCtrl.create({
                content: "Getting Cart Details ..."
            });
            loader.present();
            // Checking if user has address already
            this.params = {
                "user_id": this.user_id
            };
            this.apis.retrieve_address_details(this.params).then(function (result) {
                _this.body2 = result;
                _this.address_details = JSON.stringify(_this.body2);
                _this.address_user_id = _this.body2[0];
                _this.address_user_id.user_id;
                console.log(_this.body2);
                console.log(_this.address_details);
                console.log("USER ID " + _this.address_user_id.user_id);
                console.log("RESP CODE " + _this.address_user_id.resp_code);
            });
            loader.dismiss();
        }
        // retrieving all the cart items
        this.cartList = cartServ.getAllCartfoodItems();
        console.log("CARTLIST IN FOOD CART" + this.cartList);
        this.select = JSON.stringify(this.cartList);
        console.log("CARTLIST IN FOOD CART" + this.select);
    }
    FoodCartPage.prototype.quantityAdd = function (item) {
        this.cartServ.quantityPlus(item);
    };
    FoodCartPage.prototype.quantityMinus = function (item) {
        if (item.quantity > 1) {
            this.cartServ.quantityMinus(item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Quantity is 1, you cant reduce it, if you want to remove, please click the remove button.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    FoodCartPage.prototype.removeItemFromCart = function (item) {
        //this.cartService.removeItemById(item.id);
        var self = this;
        console.log("ITEM " + item);
        console.log("String" + JSON.stringify(item));
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to remove food item from cart?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: function () {
                        console.log('Removed');
                        self.cartServ.removeItemById(item.food_id);
                    }
                }
            ]
        });
        alert.present();
    };
    FoodCartPage.prototype.getTotal = function () {
        this.check = this.cartServ.getGrandTotal();
        console.log("GRAND TOTAL" + this.check);
        return this.cartServ.getGrandTotal();
    };
    FoodCartPage.prototype.backtoMenu = function () {
        this.navCtrl.setRoot("FoodMenuPage", { user_details: this.user_details });
    };
    FoodCartPage.prototype.checkout = function (item) {
        var _this = this;
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
            var confirm_1 = this.alertCtrl.create({
                title: '<img src = "assets/icon/danger.jpg" width="35px" height="35px"> Login Required',
                message: 'You need to either login or sign up to checkout',
                buttons: [
                    {
                        text: 'Login',
                        handler: function () {
                            var loader = _this.loadingCtrl.create({
                                content: "Redirecting to Login Page...",
                                duration: 1000
                            });
                            loader.present();
                            setTimeout(function () {
                                console.log("logging out in 1 second");
                                _this.navCtrl.push("LoginPage", { cart_or_sidemenu: "cart" });
                            }, 1000);
                            setTimeout(function () {
                                loader.dismiss();
                            }, 800);
                        }
                    },
                    {
                        text: 'Sign Up',
                        handler: function () {
                            var loader = _this.loadingCtrl.create({
                                content: "Redirecting to Signup Page...",
                                duration: 1000
                            });
                            loader.present();
                            setTimeout(function () {
                                console.log("logging out in 1 second");
                                _this.navCtrl.push("SignupPage", { cart_or_sidemenu: "cart" });
                            }, 1000);
                            setTimeout(function () {
                                loader.dismiss();
                            }, 800);
                        }
                    }
                ]
            });
            confirm_1.present();
        }
        else {
            if (this.address_user_id.user_id > 0) {
                this.navCtrl.push("FoodCheckoutPage", { user_details: this.user_details, order_id: this.order_id });
            }
            else {
                this.navCtrl.push("AddressPage", { order_id: this.order_id, user_details: this.user_details });
            }
        }
    };
    FoodCartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-cart',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-cart/food-cart.html"*/'<ion-header style="background-color: #781318">\n  <!-- <ion-navbar> -->\n  <!-- <ion-title>\n    <button\n      class="back-button disable-hover bar-button bar-button-md back-button-md bar-button-default bar-button-default-md show-back-button"\n      ion-button="bar-button"\n    >\n      <span class="button-inner"\n        ><ion-icon\n          class="back-button-icon icon icon-md back-button-icon-md ion-md-arrow-back"\n          role="img"\n          aria-label="arrow back"\n        ></ion-icon\n        ><span class="back-button-text back-button-text-md"></span\n      ></span>\n      <div class="button-effect"></div>\n    </button>\n    <p>Cart</p>\n  </ion-title> -->\n\n  <ion-row style="display: flex">\n    <ion-col col-4>\n      <button\n        class="back-button disable-hover bar-button bar-button-md back-button-md bar-button-default bar-button-default-md show-back-button"\n        ion-button="bar-button"\n        (click)="backtoMenu()"\n      >\n        <span class="button-inner"\n          ><ion-icon\n            class="back-button-icon icon icon-md back-button-icon-md ion-md-arrow-back"\n            role="img"\n            aria-label="arrow back"\n          ></ion-icon\n          ><span class="back-button-text back-button-text-md"></span\n        ></span>\n        <div class="button-effect"></div></button\n    ></ion-col>\n    <ion-col col-8> <h4 style="color: white">Cart</h4></ion-col>\n  </ion-row>\n  <!-- </ion-navbar> -->\n</ion-header>\n\n<ion-content class="background">\n  <ion-item *ngIf="cartList?.length == 0">\n    <p class="center">\n      <b>No Cart</b>\n    </p>\n  </ion-item>\n  <br />\n\n  <div *ngIf="cartList?.length > 0">\n    <ion-item *ngFor="let item of cartList" overflow-scroll="true">\n      <ion-row>\n        <ion-col col-2>\n          <img *ngIf="item" [src]="item.food_logo_url" id="logo_size" />\n        </ion-col>\n\n        <ion-col col-6 text-wrap>\n          <h4>{{ item.food_name}}</h4>\n          <p *ngIf="item.quantity > 0">\n            <!-- GHC {{ item.price * item.quantity}}  -->\n            GHC {{ item.price}}\n          </p>\n\n          <p *ngIf="item.quantity == 0">GHC {{ item.price}}</p>\n        </ion-col>\n\n        <ion-row *ngIf="item.quantity > 0">\n          <ion-col col-9>\n            <h4>\n              <p>\n                <b> Qty: {{ item.quantity}}</b>\n              </p>\n            </h4>\n          </ion-col>\n        </ion-row>\n\n        <ion-col col-1>\n          <ion-icon\n            ios="ios-close-circle"\n            md="md-close-circle"\n            color="red"\n            (tap)="removeItemFromCart(item)"\n          ></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </div>\n\n  <ion-footer>\n    <div *ngIf="cartList?.length > 0">\n      <ion-row class="ash">\n        <ion-col col-4>\n          <p style="color: #187034">CART ITEM: {{cartList.length}}</p>\n        </ion-col>\n\n        <ion-col col-8>\n          <p style="color: #187034">CART TOTAL: GHC {{getTotal()}}</p>\n        </ion-col>\n      </ion-row>\n    </div>\n\n    <p>\n      <button\n        ion-button\n        *ngIf="cartList.length"\n        color="secondary"\n        class="button button-dark customBtn"\n        (tap)="checkout(item)"\n      >\n        Proceed to checkout\n      </button>\n    </p>\n  </ion-footer>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-cart/food-cart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FoodCartPage);
    return FoodCartPage;
}());

//# sourceMappingURL=food-cart.js.map

/***/ })

});
//# sourceMappingURL=29.js.map