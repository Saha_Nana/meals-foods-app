webpackJsonp([42],{

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartsPageModule", function() { return CartsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__carts__ = __webpack_require__(338);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CartsPageModule = /** @class */ (function () {
    function CartsPageModule() {
    }
    CartsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__carts__["a" /* CartsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__carts__["a" /* CartsPage */]),
            ],
        })
    ], CartsPageModule);
    return CartsPageModule;
}());

//# sourceMappingURL=carts.module.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CartsPage = /** @class */ (function () {
    function CartsPage(toastCtrl, navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        // retrieving all the cart items
        this.cartList = cartServ.getAllCartItems();
        this.select = JSON.stringify(this.cartList);
        this.user_details = this.navParams.get("user_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
        }
        else {
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            console.log("USer DETAILS " + this.user_details);
            console.log("USer ID " + this.user_id);
            var loader_1 = this.loadingCtrl.create({
                content: "Getting Cart Details ..."
            });
            loader_1.present();
            // Checking if user has address already
            this.params = {
                "user_id": this.user_id
            };
            this.apis.retrieve_address_details(this.params).then(function (result) {
                _this.body2 = result;
                _this.address_details = JSON.stringify(_this.body2);
                _this.address_user_id = _this.body2[0];
                console.log(_this.body2);
                console.log(_this.address_details);
                console.log("USER ID " + _this.address_user_id.user_id);
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ['OK']
                });
                alert.present();
                _this.toastCtrl.create({
                    message: "Please check your internet connection",
                    duration: 5000
                }).present();
                loader_1.dismiss();
                console.log(err);
            });
            loader_1.dismiss();
        }
    }
    CartsPage.prototype.quantityAdd = function (item) {
        this.cartServ.quantityPlus(item);
    };
    CartsPage.prototype.quantityMinus = function (item) {
        if (item.quantity > 1) {
            this.cartServ.quantityMinus(item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Quantity is 1, you cant reduce it, if you want to remove, please press remove button.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    CartsPage.prototype.removeItemFromCart = function (item) {
        //this.cartService.removeItemById(item.id);
        var self = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to remove food item from cart?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: function () {
                        console.log('Buy clicked');
                        self.cartServ.removeItemById(item.id);
                    }
                }
            ]
        });
        alert.present();
    };
    CartsPage.prototype.getTotal = function () {
        this.check = this.cartServ.getGrandTotal();
        return this.cartServ.getGrandTotal();
    };
    CartsPage.prototype.checkout = function (item) {
        var _this = this;
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
            var confirm_1 = this.alertCtrl.create({
                title: '<img src = "assets/icon/danger.jpg" width="35px" height="35px"> Login Required',
                message: 'You need to either login or sign up to checkout',
                buttons: [
                    {
                        text: 'Login',
                        handler: function () {
                            var loader = _this.loadingCtrl.create({
                                content: "Redirecting to Login Page...",
                                duration: 1000
                            });
                            loader.present();
                            setTimeout(function () {
                                console.log("logging out in 1 second");
                                _this.navCtrl.push("LoginPage", { cart_or_sidemenu: "meals_cart" });
                            }, 1000);
                            setTimeout(function () {
                                loader.dismiss();
                            }, 800);
                        }
                    },
                    {
                        text: 'Sign Up',
                        handler: function () {
                            var loader = _this.loadingCtrl.create({
                                content: "Redirecting to Signup Page...",
                                duration: 1000
                            });
                            loader.present();
                            setTimeout(function () {
                                console.log("logging out in 1 second");
                                _this.navCtrl.push("SignupPage", { cart_or_sidemenu: "meals_cart" });
                            }, 1000);
                            setTimeout(function () {
                                loader.dismiss();
                            }, 800);
                        }
                    }
                ]
            });
            confirm_1.present();
        }
        else {
            if (this.address_user_id.user_id > 0) {
                this.navCtrl.push("CheckoutPage", { user_details: this.user_details, order_id: this.order_id });
            }
            else {
                this.navCtrl.push("AddressPage", { order_id: this.order_id, user_details: this.user_details });
            }
        }
    };
    CartsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-carts',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/carts/carts.html"*/'<ion-header>\n  <ion-navbar>\n\n    <ion-title>\n      <p>Cart</p>\n    </ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="background">\n  <ion-item *ngIf="cartList?.length == 0">\n\n    <p class="center">\n      <b>No Cart</b>\n    </p>\n  </ion-item>\n  <br>\n\n\n  <div *ngIf="cartList?.length > 0">\n    <ion-item *ngFor="let item of cartList">\n      <ion-row>\n        <ion-col col-2>\n          <img *ngIf="item" [src]="item.meal_logo_url" id="logo_size" />        \n        </ion-col>\n\n        <ion-col col-6 text-wrap>\n          <h4>\n            {{ item.meal_name}}\n          </h4>\n          <p>\n            GHC {{ item.price}}\n          </p>\n        </ion-col>\n      <!-- </ion-row>\n\n        <ion-row> -->\n\n        <ion-col col-1>\n          <!-- <button id="font" ion-button color="kaya"  (click)="quantityMinus(item)"> -->\n            <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger" (tap)="quantityMinus(item)"></ion-icon>\n          <!-- </button> -->\n        </ion-col>\n\n        <ion-col col-1>\n          <h4>\n            <p>\n              <b>{{ item.quantity}}</b>\n            </p>\n          </h4>\n        </ion-col>\n\n        <ion-col col-1>\n          <!-- <button id="font" ion-button color="secondary" (click)="quantityAdd(item)"> -->\n            <ion-icon ios="ios-add-circle" md="md-add-circle" color="secondary" (tap)="quantityAdd(item)"></ion-icon>\n          <!-- </button> -->\n        </ion-col>\n\n        <ion-col col-1>\n          <!-- <button  id="font" ion-button color="danger" (click)="removeItemFromCart(item)"> -->\n            <ion-icon ios="ios-close-circle" md="md-close-circle"  color="red" (tap)="removeItemFromCart(item)"></ion-icon>\n          <!-- </button> -->\n        </ion-col>\n       \n      </ion-row>\n\n\n    </ion-item>\n  </div>\n\n\n  <ion-footer>\n    <div *ngIf="cartList?.length > 0">\n\n      <ion-row class="ash">\n\n        <ion-col col-6 text-wrap>\n          <p style=" color: #187034;">CART ITEM: {{cartList.length}}</p>\n\n        </ion-col>\n\n        <ion-col col-6 text-wrap>\n          <p style=" color: #187034;">CART TOTAL: GHC {{getTotal()}}</p>\n\n        </ion-col>\n\n      </ion-row>\n    </div>\n\n    <p>\n      <button ion-button *ngIf="cartList.length" color="secondary" class="button button-dark customBtn" (tap)="checkout(item)">Proceed to checkout</button>\n    </p>\n  </ion-footer>\n\n</ion-content>\n\n<!-- <ion-footer>\n  <div>\n\n    <ion-row class="ash">\n\n      <ion-col col-6>\n        <p style=" color: #187034;">CART ITEM: {{4}}</p>\n\n      </ion-col>\n\n      <ion-col col-6>\n        <p style=" color: #187034;">CART TOTAL: GHC {{590}}</p>\n\n      </ion-col>\n\n    </ion-row>\n  </div>\n\n  <p>\n    <button ion-button color="secondary"  class="button button-dark customBtn" (tap)="address()">Proceed to checkout</button>\n  </p>\n\n</ion-footer> -->'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/carts/carts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["b" /* CartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], CartsPage);
    return CartsPage;
}());

//# sourceMappingURL=carts.js.map

/***/ })

});
//# sourceMappingURL=42.js.map