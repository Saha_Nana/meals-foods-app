webpackJsonp([44],{

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/address/address.module": [
		282,
		43
	],
	"../pages/carts/carts.module": [
		294,
		42
	],
	"../pages/catering-cart/catering-cart.module": [
		284,
		41
	],
	"../pages/catering-checkout/catering-checkout.module": [
		283,
		40
	],
	"../pages/catering-item-details/catering-item-details.module": [
		285,
		39
	],
	"../pages/catering-menu/catering-menu.module": [
		286,
		38
	],
	"../pages/catering-order-details/catering-order-details.module": [
		287,
		37
	],
	"../pages/catering-payment/catering-payment.module": [
		288,
		36
	],
	"../pages/cateringlist/cateringlist.module": [
		289,
		35
	],
	"../pages/changepassword/changepassword.module": [
		290,
		34
	],
	"../pages/checkout/checkout.module": [
		291,
		33
	],
	"../pages/contact/contact.module": [
		292,
		32
	],
	"../pages/createlist/createlist.module": [
		293,
		31
	],
	"../pages/feedback/feedback.module": [
		295,
		30
	],
	"../pages/food-cart/food-cart.module": [
		296,
		29
	],
	"../pages/food-checkout/food-checkout.module": [
		297,
		28
	],
	"../pages/food-details/food-details.module": [
		298,
		27
	],
	"../pages/food-menu/food-menu.module": [
		299,
		26
	],
	"../pages/food-order-details/food-order-details.module": [
		300,
		25
	],
	"../pages/food-payment/food-payment.module": [
		301,
		24
	],
	"../pages/food-receipt/food-receipt.module": [
		302,
		23
	],
	"../pages/forgotpassword/forgotpassword.module": [
		303,
		22
	],
	"../pages/home/home.module": [
		304,
		21
	],
	"../pages/info/info.module": [
		305,
		20
	],
	"../pages/item-details/item-details.module": [
		306,
		19
	],
	"../pages/list/list.module": [
		307,
		18
	],
	"../pages/login/login.module": [
		308,
		17
	],
	"../pages/loyalty/loyalty.module": [
		309,
		16
	],
	"../pages/map/map.module": [
		310,
		15
	],
	"../pages/meal-menu/meal-menu.module": [
		311,
		14
	],
	"../pages/order-details/order-details.module": [
		312,
		13
	],
	"../pages/order-history/order-history.module": [
		313,
		12
	],
	"../pages/payment-history/payment-history.module": [
		314,
		11
	],
	"../pages/payment/payment.module": [
		316,
		10
	],
	"../pages/profile-edit/profile-edit.module": [
		315,
		9
	],
	"../pages/profile/profile.module": [
		317,
		8
	],
	"../pages/receipt/receipt.module": [
		318,
		7
	],
	"../pages/resetcode/resetcode.module": [
		319,
		6
	],
	"../pages/restaurants/restaurants.module": [
		320,
		5
	],
	"../pages/search-meals/search-meals.module": [
		321,
		4
	],
	"../pages/searchcatering/searchcatering.module": [
		323,
		3
	],
	"../pages/signup/signup.module": [
		322,
		2
	],
	"../pages/sliders/sliders.module": [
		324,
		1
	],
	"../pages/tracking/tracking.module": [
		325,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 153;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApisProvider; });
/* unused harmony export CateringMenuItem */
/* unused harmony export CateringCartItem */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return CateringCartService; });
/* unused harmony export MenuItem */
/* unused harmony export CartItem */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CartService; });
/* unused harmony export FoodMenuItem */
/* unused harmony export FoodCartItem */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return FoodCartService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApisProvider = /** @class */ (function () {
    function ApisProvider(http) {
        this.http = http;
        this.reset_password_get_phonenumber_url = 'https://api.mealsandfood.com/reset_password_get_phonenumber';
        this.reset_password_get_smscode_url = "https://api.mealsandfood.com/reset_password_get_reset_token";
        this.reset_password_new_password_url = "https://api.mealsandfood.com/reset_password_new_password";
        this.update_profile_url = 'https://api.mealsandfood.com/update_user_profile';
        this.send_feedback_url = 'https://api.mealsandfood.com/give_feedback';
        this.signup_url = 'https://api.mealsandfood.com/account_creation';
        this.login_url = 'https://api.mealsandfood.com/account_access';
        this.retrieve_url = 'https://api.mealsandfood.com/retrieve_details';
        this.retrieve_all_meals_url = 'https://api.mealsandfood.com/fetch_all_meals';
        this.retrieve_all_menu_url = 'https://api.mealsandfood.com/fetch_all_menu';
        this.retrieve_all_locations_url = 'https://api.mealsandfood.com/fetch_all_location';
        this.fetch_restaurants_url = 'https://api.mealsandfood.com/fetch_restaurants/';
        this.fetch_caterers_url = 'https://api.mealsandfood.com/fetch_caterers/';
        this.fetch_restaurants_menu_url = 'https://api.mealsandfood.com/restaurant_menu/';
        this.fetch_caterers_menu_url = 'https://api.mealsandfood.com/caterer_menu/';
        this.retrieve_restaurant_menu_url = 'https://api.mealsandfood.com/retrieve_restaurant_menu';
        this.meal_details_url = 'https://api.mealsandfood.com/meal_details';
        this.food_details_url = 'https://api.mealsandfood.com/food_details';
        this.process_orders_url = 'https://api.mealsandfood.com/process_orders';
        this.process_food_orders_url = 'https://api.mealsandfood.com/process_food_orders';
        this.process_catering_orders_url = 'https://api.mealsandfood.com/process_catering_orders';
        this.save_address_url = 'https://api.mealsandfood.com/save_address';
        this.update_address_url = 'https://api.mealsandfood.com/update_address';
        this.update_list_url = 'https://api.mealsandfood.com/update_list';
        this.retrieve_address_details_url = 'https://api.mealsandfood.com/retrieve_address_details';
        this.order_details_url = 'https://api.mealsandfood.com/displayOrderDetails';
        this.food_order_details_url = 'https://api.mealsandfood.com/displayFoodOrderDetails';
        this.catering_order_details_url = 'https://api.mealsandfood.com/displayCateringOrderDetails';
        this.order_history_url = 'https://api.mealsandfood.com/displayOrders';
        this.food_order_history_url = 'https://api.mealsandfood.com/displayFoodOrders';
        this.catering_order_history_url = 'https://api.mealsandfood.com/displayCateringOrders';
        this.fetch_foodstuff_menus_url = 'https://api.mealsandfood.com/food_menu';
        this.track_order_url = 'https://api.mealsandfood.com/track_order';
        this.create_list_url = 'https://api.mealsandfood.com/create_list';
        this.retrieve_list_details_url = 'https://api.mealsandfood.com/retrieve_list_details';
        this.place_weekly_order_url = 'https://api.mealsandfood.com/place_weekly_order';
        this.delete_schedule_url = 'https://api.mealsandfood.com/delete_list_item';
        this.order_payment_url = 'https://api.mealsandfood.com/order_payment';
        this.payment_history_url = 'https://api.mealsandfood.com/payment_history';
        this.save_special_food_requests_url = 'https://api.mealsandfood.com/save_food_special_requests';
        this.retrieve_all_foods_url = 'https://api.mealsandfood.com/fetch_all_foods';
        this.retrieve_delivery_charges_url = 'https://api.mealsandfood.com/retrieve_delivery_charge';
        this.retrieve_loyalty_url = 'https://api.mealsandfood.com/loyalty_points';
        this.time_slot_url = 'https://api.mealsandfood.com/time_slot';
        console.log('Hello ApisProvider Provider');
    }
    ApisProvider.prototype.save_special_request_food = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.save_special_food_requests_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.track_order = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.track_order_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.make_payment = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.order_payment_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.payment_history = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.payment_history_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.signup = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.signup_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.login = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.login_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.retrieve_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_loyalty_points = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.retrieve_loyalty_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_all_meals = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.retrieve_all_meals_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_all_menu = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.retrieve_all_menu_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_time_slot = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.time_slot_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_all_locations = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.retrieve_all_locations_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_all_foods = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.retrieve_all_foods_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_delivery_charges = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.retrieve_delivery_charges_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.fetch_restaurants = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.fetch_restaurants_url + data, data)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.fetch_caterers = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.fetch_caterers_url + data, data)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.fetch_restaurant_menus = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.fetch_restaurants_menu_url + data, data)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.fetch_catering_menus = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.fetch_caterers_menu_url + data, data)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.fetch_foodstuff_menus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.fetch_foodstuff_menus_url)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_restaurant_menu = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.retrieve_restaurant_menu_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.meal_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.meal_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.food_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.food_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.process_order = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.process_orders_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.process_food_order = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.process_food_orders_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.process_catering_order = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.process_catering_orders_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.order_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.order_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.food_order_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.food_order_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.catering_order_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.catering_order_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.order_history = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.order_history_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.food_order_history = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.food_order_history_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.catering_order_history = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.catering_order_history_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.save_address = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.save_address_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.update_address = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.update_address_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.update_list = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.update_list_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_address_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.retrieve_address_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.create_list = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.create_list_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.retrieve_list_details = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.retrieve_list_details_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.place_weekly_order = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.place_weekly_order_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.delete_schedule = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.delete_schedule_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.reset_password_get_phonenumber = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.reset_password_get_phonenumber_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(console.log(JSON.stringify(err)));
            });
        });
    };
    ApisProvider.prototype.reset_password_get_smscode = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.reset_password_get_smscode_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(console.log(JSON.stringify(err)));
            });
        });
    };
    ApisProvider.prototype.reset_password_new_password = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.reset_password_new_password_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(console.log(JSON.stringify(err)));
            });
        });
    };
    ApisProvider.prototype.update_profile = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.update_profile_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider.prototype.send_feedback = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.send_feedback_url, JSON.stringify(data))
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApisProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ApisProvider);
    return ApisProvider;
}());

// CATERING CART SERVICES
var CateringMenuItem = /** @class */ (function () {
    function CateringMenuItem(id, catering_meals_id, meal_name, price, discount, meal_logo_url) {
        this.id = id;
        this.catering_meals_id = catering_meals_id;
        this.meal_name = meal_name;
        this.price = price;
        this.discount = discount;
        this.meal_logo_url = meal_logo_url;
    }
    return CateringMenuItem;
}());

var CateringCartItem = /** @class */ (function () {
    function CateringCartItem(item, quantity) {
        this.id = item.id;
        this.catering_meals_id = item.catering_meals_id;
        this.meal_name = item.meal_name;
        this.price = item.price;
        this.discount = item.discount;
        this.quantity = quantity;
        this.meal_logo_url = item.meal_logo_url;
    }
    return CateringCartItem;
}());

var CateringCartService = /** @class */ (function () {
    function CateringCartService() {
        this.list = [];
    }
    CateringCartService.prototype.getAllCateringCartItems = function () {
        return this.list;
    };
    CateringCartService.prototype.addItem = function (product, quantity) {
        var isExists = false;
        // var id = product.id;
        var id = product.catering_meals_id;
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].catering_meals_id == id) {
                this.list[i].quantity += quantity;
                isExists = true;
                break;
            }
        }
        if (!isExists) {
            this.list.push(new CateringCartItem(product, quantity));
        }
    };
    CateringCartService.prototype.quantityPlus = function (item) {
        item.quantity += 1.00;
    };
    CateringCartService.prototype.quantityMinus = function (item) {
        item.quantity -= 1.00;
    };
    CateringCartService.prototype.removeItemById = function (id) {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].id == id) {
                this.list.splice(i, 1);
                break;
            }
        }
    };
    CateringCartService.prototype.removeallcart = function () {
        // return  this.list = []
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i]) {
                this.list.splice(i, 0);
                return this.list = [];
            }
        }
    };
    CateringCartService.prototype.getGrandTotal = function () {
        var amount = 0;
        for (var i = 0; i < this.list.length; i++) {
            amount += (this.list[i].price * this.list[i].quantity);
        }
        return amount;
    };
    CateringCartService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], CateringCartService);
    return CateringCartService;
}());

//CETERIN CART END
var MenuItem = /** @class */ (function () {
    function MenuItem(id, meals_id, meal_name, price, discount, meal_logo_url) {
        this.id = id;
        this.meals_id = meals_id;
        this.meal_name = meal_name;
        this.price = price;
        this.discount = discount;
        this.meal_logo_url = meal_logo_url;
    }
    return MenuItem;
}());

var CartItem = /** @class */ (function () {
    function CartItem(item, quantity) {
        this.id = item.id;
        this.meals_id = item.meals_id;
        this.meal_name = item.meal_name;
        this.price = item.price;
        this.discount = item.discount;
        this.quantity = quantity;
        this.meal_logo_url = item.meal_logo_url;
    }
    return CartItem;
}());

var CartService = /** @class */ (function () {
    function CartService() {
        this.list = [];
    }
    CartService.prototype.getAllCartItems = function () {
        return this.list;
    };
    CartService.prototype.addItem = function (product, quantity) {
        var isExists = false;
        // var id = product.id;
        var id = product.meals_id;
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].meals_id == id) {
                this.list[i].quantity += quantity;
                isExists = true;
                break;
            }
        }
        if (!isExists) {
            this.list.push(new CartItem(product, quantity));
        }
    };
    CartService.prototype.quantityPlus = function (item) {
        item.quantity += 1.00;
    };
    CartService.prototype.quantityMinus = function (item) {
        item.quantity -= 1.00;
    };
    CartService.prototype.removeItemById = function (id) {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].id == id) {
                this.list.splice(i, 1);
                break;
            }
        }
    };
    CartService.prototype.removeallcart = function () {
        // return  this.list = []
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i]) {
                this.list.splice(i, 0);
                return this.list = [];
            }
        }
    };
    CartService.prototype.getGrandTotal = function () {
        var amount = 0;
        for (var i = 0; i < this.list.length; i++) {
            amount += (this.list[i].price * this.list[i].quantity);
        }
        return amount;
    };
    CartService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], CartService);
    return CartService;
}());

// FOODSTUFFS CART SERVICES
var FoodMenuItem = /** @class */ (function () {
    function FoodMenuItem(id, food_id, food_name, price, discount, food_logo_url) {
        this.id = id;
        this.food_id = food_id;
        this.food_name = food_name;
        this.price = price;
        this.discount = discount;
        this.food_logo_url = food_logo_url;
    }
    return FoodMenuItem;
}());

var FoodCartItem = /** @class */ (function () {
    function FoodCartItem(item, quantity) {
        this.id = item.id;
        this.food_id = item.food_id;
        this.food_name = item.food_name;
        this.price = item.price;
        this.discount = item.discount;
        this.quantity = quantity;
        this.food_logo_url = item.food_logo_url;
    }
    return FoodCartItem;
}());

var FoodCartService = /** @class */ (function () {
    function FoodCartService() {
        this.list = [];
    }
    FoodCartService.prototype.getAllCartfoodItems = function () {
        return this.list;
    };
    FoodCartService.prototype.addItem = function (product, quantity) {
        var isExists = false;
        // var id = product.id;
        var id = product.food_id;
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].food_id == id) {
                this.list[i].quantity += quantity;
                isExists = true;
                break;
            }
        }
        if (!isExists) {
            this.list.push(new FoodCartItem(product, quantity));
        }
    };
    FoodCartService.prototype.quantityPlus = function (item) {
        item.quantity += 1;
    };
    FoodCartService.prototype.quantityMinus = function (item) {
        item.quantity -= 1;
    };
    // removeItemById(id) {
    //   for (var i = 0; i < this.list.length; i++) {
    //     if (this.list[i].id == id) {
    //       this.list.splice(i, 1);
    //       break;
    //     }
    //   }
    // }
    FoodCartService.prototype.removeItemById = function (item) {
        var id = item;
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].food_id == id) {
                this.list.splice(i, 1);
                break;
            }
        }
    };
    FoodCartService.prototype.removeallfoodcart = function () {
        // return  this.list = []
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i]) {
                this.list.splice(i, 0);
                return this.list = [];
            }
        }
    };
    FoodCartService.prototype.getGrandTotal = function () {
        var amount = 0;
        for (var i = 0; i < this.list.length; i++) {
            amount += (this.list[i].price);
        }
        return amount;
    };
    FoodCartService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], FoodCartService);
    return FoodCartService;
}());

//# sourceMappingURL=apis.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(226);



// this is the magic wand
Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_14" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular4_paystack__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// import { OneSignal } from '@ionic-native/onesignal'; 

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__["a" /* IonicSelectableModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/address/address.module#AddressPageModule', name: 'AddressPage', segment: 'address', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-checkout/catering-checkout.module#CateringCheckoutPageModule', name: 'CateringCheckoutPage', segment: 'catering-checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-cart/catering-cart.module#CateringCartPageModule', name: 'CateringCartPage', segment: 'catering-cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-item-details/catering-item-details.module#CateringItemDetailsPageModule', name: 'CateringItemDetailsPage', segment: 'catering-item-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-menu/catering-menu.module#CateringMenuPageModule', name: 'CateringMenuPage', segment: 'catering-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-order-details/catering-order-details.module#CateringOrderDetailsPageModule', name: 'CateringOrderDetailsPage', segment: 'catering-order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/catering-payment/catering-payment.module#CateringPaymentPageModule', name: 'CateringPaymentPage', segment: 'catering-payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cateringlist/cateringlist.module#CateringlistPageModule', name: 'CateringlistPage', segment: 'cateringlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/changepassword/changepassword.module#ChangepasswordPageModule', name: 'ChangepasswordPage', segment: 'changepassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/checkout/checkout.module#CheckoutPageModule', name: 'CheckoutPage', segment: 'checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createlist/createlist.module#CreatelistPageModule', name: 'CreatelistPage', segment: 'createlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/carts/carts.module#CartsPageModule', name: 'CartsPage', segment: 'carts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-cart/food-cart.module#FoodCartPageModule', name: 'FoodCartPage', segment: 'food-cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-checkout/food-checkout.module#FoodCheckoutPageModule', name: 'FoodCheckoutPage', segment: 'food-checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-details/food-details.module#FoodDetailsPageModule', name: 'FoodDetailsPage', segment: 'food-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-menu/food-menu.module#FoodMenuPageModule', name: 'FoodMenuPage', segment: 'food-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-order-details/food-order-details.module#FoodOrderDetailsPageModule', name: 'FoodOrderDetailsPage', segment: 'food-order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-payment/food-payment.module#FoodPaymentPageModule', name: 'FoodPaymentPage', segment: 'food-payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/food-receipt/food-receipt.module#FoodReceiptPageModule', name: 'FoodReceiptPage', segment: 'food-receipt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgotpassword/forgotpassword.module#ForgotpasswordPageModule', name: 'ForgotpasswordPage', segment: 'forgotpassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/item-details/item-details.module#ItemDetailsPageModule', name: 'ItemDetailsPage', segment: 'item-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list/list.module#ListPageModule', name: 'ListPage', segment: 'list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loyalty/loyalty.module#LoyaltyPageModule', name: 'LoyaltyPage', segment: 'loyalty', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meal-menu/meal-menu.module#MealMenuPageModule', name: 'MealMenuPage', segment: 'meal-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-details/order-details.module#OrderDetailsPageModule', name: 'OrderDetailsPage', segment: 'order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-history/order-history.module#OrderHistoryPageModule', name: 'OrderHistoryPage', segment: 'order-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment-history/payment-history.module#PaymentHistoryPageModule', name: 'PaymentHistoryPage', segment: 'payment-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-edit/profile-edit.module#ProfileEditPageModule', name: 'ProfileEditPage', segment: 'profile-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/receipt/receipt.module#ReceiptPageModule', name: 'ReceiptPage', segment: 'receipt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resetcode/resetcode.module#ResetcodePageModule', name: 'ResetcodePage', segment: 'resetcode', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/restaurants/restaurants.module#RestaurantsPageModule', name: 'RestaurantsPage', segment: 'restaurants', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-meals/search-meals.module#SearchMealsPageModule', name: 'SearchMealsPage', segment: 'search-meals', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/searchcatering/searchcatering.module#SearchcateringPageModule', name: 'SearchcateringPage', segment: 'searchcatering', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sliders/sliders.module#SlidersPageModule', name: 'SlidersPage', segment: 'sliders', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tracking/tracking.module#TrackingPageModule', name: 'TrackingPage', segment: 'tracking', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_12_angular4_paystack__["a" /* Angular4PaystackModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_10__providers_apis_apis__["a" /* ApisProvider */],
                __WEBPACK_IMPORTED_MODULE_10__providers_apis_apis__["b" /* CartService */],
                __WEBPACK_IMPORTED_MODULE_10__providers_apis_apis__["d" /* FoodCartService */],
                __WEBPACK_IMPORTED_MODULE_10__providers_apis_apis__["c" /* CateringCartService */]
                // OneSignal
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { OneSignal } from '@ionic-native/onesignal';
var MyApp = /** @class */ (function () {
    function MyApp(loadingCtrl, alertCtrl, event, platform, statusBar, splashScreen) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.event = event;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = "SlidersPage";
        this.signal_app_id = '8536d8e3-ae88-4e02-bd20-7f9e9563081b';
        this.firebase_sender_id = '564321132968';
        this.initializeApp();
        this.event.subscribe('customer_details', function (data) {
            _this.customer_details = data;
            console.log(_this.customer_details);
            //Checking if user data is shown / logged in
            if (_this.customer_details == undefined) {
                console.log("No Login - NO User Data");
                _this.pages = [
                    { title: 'Home', component: "HomePage", feature: "dev", icon: "ios-home" },
                    { title: 'Contact M&F', component: "ContactPage", feature: "dev", icon: "ios-pizza" },
                ];
            }
            else {
                console.log("CUSTOMER DETAILS HERE" + _this.customer_details);
                console.log(data);
                _this.jsonBody = JSON.parse(_this.customer_details);
                _this.first_name = _this.jsonBody[0].first_name;
                _this.last_name = _this.jsonBody[0].last_name;
                console.log("FIRST NAME" + _this.first_name);
                _this.pages = [
                    { title: 'Home', component: "HomePage", feature: "dev", icon: "ios-home" },
                    { title: 'Orders', component: "OrderHistoryPage", feature: "dev", icon: "md-cart" },
                    { title: 'Payments History', component: "PaymentHistoryPage", feature: "dev", icon: "md-cash" },
                    { title: 'Create Lists', component: "ListPage", feature: "dev", icon: "ios-pizza" },
                    { title: 'Give Feedback', component: "FeedbackPage", feature: "dev", icon: "ios-pizza" },
                    { title: 'Contact M&F', component: "ContactPage", feature: "dev", icon: "ios-pizza" },
                ];
            }
        });
        // used for an example of ngFor and navigation
        if (this.customer_details == undefined) {
        }
        else {
        }
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            // this.oneSignal.startInit(this.signal_app_id, this.firebase_sender_id);
            // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            // this.oneSignal.handleNotificationReceived().subscribe((res) => {
            //   // do something when notification is received
            //   console.log(res)
            // });
            // this.oneSignal.handleNotificationOpened().subscribe((res) => {
            //   // do something when a notification is opened
            //   console.log(res)
            // });
            // this.oneSignal.endInit();
        });
    };
    MyApp.prototype.edit = function (item) {
        this.nav.setRoot("ProfilePage", { customer_details: this.customer_details });
    };
    MyApp.prototype.login = function (item) {
        this.nav.setRoot("LoginPage", { cart_or_sidemenu: "sidemenu" });
    };
    MyApp.prototype.openPage = function (page) {
        console.log(page);
        if (page.component) {
            // this.nav.push(page.component);
            this.nav.push(page, { 'customer_details': this.customer_details });
        }
        else {
            // Since the component is null, this is the logout option
            console.log("feature = " + page.feature + " AND title = " + page.title);
            if (page.feature == "done") {
                this.logout_func();
            }
        }
    };
    MyApp.prototype.logout_func = function () {
        var _this = this;
        // logout logic
        var confirm = this.alertCtrl.create({
            title: 'Logout',
            message: 'Do you wish to logout?',
            buttons: [
                {
                    text: 'No',
                    handler: function () { }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        var loader = _this.loadingCtrl.create({
                            content: "Logging out...",
                            duration: 1000
                        });
                        loader.present();
                        setTimeout(function () {
                            console.log("logging out in 1 second");
                            _this.nav.setRoot("LoginPage", { cart_or_sidemenu: "sidemenu" });
                        }, 1000);
                        setTimeout(function () {
                            loader.dismiss();
                        }, 800);
                    }
                }
            ]
        });
        confirm.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-content id="background2">\n    <div class="padding-icon">\n      <img src="assets/icon/male.png" width="30%" />\n    </div>\n\n    <ion-list *ngIf="customer_details == undefined">\n      <button ion-button color="primary" outline (tap)="login()">\n        Click to Login / Signup\n      </button>\n    </ion-list>\n\n    <ion-list *ngIf="customer_details != undefined">\n      <p class="down1">{{first_name}} {{last_name}}</p>\n      <button ion-button color="primary" outline (tap)="edit(item)">\n        Edit Profile\n      </button>\n      <button ion-button color="secondary" outline (tap)="logout_func()">\n        Logout\n      </button>\n    </ion-list>\n\n    <ion-list class="sidelist">\n      <button\n        menuClose\n        ion-item\n        id="background2"\n        class="pad"\n        *ngFor="let p of pages"\n        (click)="openPage(p)"\n      >\n        <!-- <ion-icon [name]="p.icon" item-left></ion-icon> -->\n        {{p.title}}\n      </button>\n\n      <ion-footer>\n        <p class="footer-font">\n          Designed & Developed By\n          <span>\n            <a class="footer-font2" href="https://www.devdexsoftware.com"\n              >Devdex Software\n            </a>\n          </span>\n        </p></ion-footer\n      >\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map