webpackJsonp([23],{

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodReceiptPageModule", function() { return FoodReceiptPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_receipt__ = __webpack_require__(346);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodReceiptPageModule = /** @class */ (function () {
    function FoodReceiptPageModule() {
    }
    FoodReceiptPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_receipt__["a" /* FoodReceiptPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_receipt__["a" /* FoodReceiptPage */]),
            ],
        })
    ], FoodReceiptPageModule);
    return FoodReceiptPageModule;
}());

//# sourceMappingURL=food-receipt.module.js.map

/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodReceiptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FoodReceiptPage = /** @class */ (function () {
    function FoodReceiptPage(navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.order_id = this.navParams.get("order_id");
        this.user_details = this.navParams.get("user_details");
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        console.log("USer DETAILS " + this.user_details);
        console.log("USer ID " + this.user_id);
        this.cartList = cartServ.getAllCartfoodItems();
        console.log(this.cartList);
    }
    FoodReceiptPage.prototype.getTotal = function () {
        this.check = this.cartServ.getGrandTotal();
        return this.cartServ.getGrandTotal();
    };
    FoodReceiptPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    FoodReceiptPage.prototype.order_history = function () {
        this.navCtrl.push("OrderHistoryPage", { customer_details: this.user_details });
    };
    FoodReceiptPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-receipt',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-receipt/food-receipt.html"*/'\n<ion-header>\n\n    <ion-navbar>\n      <ion-title>Receipt</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content class="background" >\n  \n      <ion-item no-lines class="center">\n          <p> Congratulations!! <br>Your Order has been received!</p>\n          \n        </ion-item>\n      <ion-item no-lines class="center">\n          <img src="assets/imgs/invoice2.svg" class="imgsize" />\n        </ion-item>\n  \n        <ion-item no-lines class="center">\n            \n            <p>  Total Order is GHc {{getTotal() | number:\'1.2-2\'}}</p>\n            <p> Order ID is {{order_id}}</p>\n          </ion-item>\n  \n          \n          \n  \n  </ion-content>\n  \n  <ion-footer>\n      <div >\n  \n        <ion-row >\n  \n          <ion-col col-6>\n              <p>\n                  <button ion-button color="secondary" outline class="button button-dark customBtn" (tap)="home()">Continue Order</button>\n                </p>\n  \n          </ion-col>\n  \n          <ion-col col-6>\n              <p>\n                  <button ion-button color="secondary" class="button button-dark customBtn" (tap)="order_history()">Check Order Status</button>\n                </p>\n  \n          </ion-col>\n  \n        </ion-row>\n      </div>\n  \n     \n    </ion-footer>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-receipt/food-receipt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FoodReceiptPage);
    return FoodReceiptPage;
}());

//# sourceMappingURL=food-receipt.js.map

/***/ })

});
//# sourceMappingURL=23.js.map