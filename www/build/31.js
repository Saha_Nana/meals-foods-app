webpackJsonp([31],{

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatelistPageModule", function() { return CreatelistPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__createlist__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CreatelistPageModule = /** @class */ (function () {
    function CreatelistPageModule() {
    }
    CreatelistPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__createlist__["a" /* CreatelistPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__createlist__["a" /* CreatelistPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_selectable__["a" /* IonicSelectableModule */]
            ],
        })
    ], CreatelistPageModule);
    return CreatelistPageModule;
}());

//# sourceMappingURL=createlist.module.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreatelistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreatelistPage = /** @class */ (function () {
    function CreatelistPage(toastCtrl, apis, _form, loadingCtrl, alertCtrl, navCtrl, navParams, http) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.apis = apis;
        this._form = _form;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.list_items = { food_name: '', amount: '' };
        this.user_details = this.navParams.get("customer_details");
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.retrieve_all_foods().then(function (result) {
            _this.food_items = result;
            console.log(_this.food_items);
            console.log(JSON.stringify(_this.food_items));
            console.log("Information", _this.food_items);
            console.log("-------------------------------------------------------");
        });
        loader.dismiss();
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        console.log("USer DETAILS From LOGIN" + this.user_details);
        console.log("USer ID " + this.user_id);
        this.listForm = this._form.group({
            "food_name": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "amount": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
    }
    CreatelistPage.prototype.foodChange = function (event) {
        this.food = event.value;
        console.log('Meal Id:', this.food.id);
        console.log('Meal Name:', this.food.name);
    };
    CreatelistPage.prototype.addtoList = function () {
        var _this = this;
        this.formValues = JSON.stringify(this.listForm.value);
        console.log("LETS SEE THE FORM VALUSE" + this.formValues);
        this.jsonBody = JSON.parse(this.formValues);
        console.log("THIS IS THE FORM VALUSE" + this.jsonBody);
        console.log("THIS IS THE FORM VALUSE STRINGIFY" + JSON.stringify(this.jsonBody));
        this.params = {
            "user_id": this.user_id,
            "food_name": this.food.name,
            "amount": this.jsonBody.amount,
            "list_type": "Weekly"
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        if (this.navParams.get('listDetails')) {
            this.apis.update_list(this.params).then(function (result) {
                _this.body1 = result;
                console.log("UPDATE LIST RESULTS" + result);
                console.log("UPDATE LIST RESULTS" + _this.body1);
            });
        }
        else {
            this.apis.create_list(this.params).then(function (result) {
                console.log("Results of list created" + result);
                console.log(result);
            });
            loader.dismiss();
            this.navCtrl.setRoot("ListPage", { customer_details: this.user_details });
        }
    };
    CreatelistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createlist',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/createlist/createlist.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Create Weekly List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="background">\n  <form [formGroup]="listForm">\n    <ion-list>\n      <p>\n        <b style="color: black">Enter Food name</b>\n      </p>\n    </ion-list>\n    <ion-item>\n      <ion-label></ion-label>\n      <ionic-selectable\n        item-content\n        [(ngModel)]="list_items.food_name"\n        [items]="food_items"\n        itemValueField="id"\n        itemTextField="name"\n        [canSearch]="true"\n        [canClear]="true"\n        (onChange)="foodChange($event)"\n        formControlName="food_name"\n        name="food_name"\n        [class.invalid]="!listForm.controls.food_name.valid && (listForm.controls.food_name.dirty || submitAttempt)"\n      >\n      </ionic-selectable>\n    </ion-item>\n    <br />\n    <!-- <ion-item>\n      <ion-label stacked>Enter Food name</ion-label>\n      <ion-input type="text" placeholder="Foodstuff name" formControlName="food_name" name="food_name" \n        [class.invalid]="!listForm.controls.food_name.valid && (listForm.controls.food_name.dirty || submitAttempt)"></ion-input>\n    </ion-item> -->\n\n    <ion-list>\n      <p>\n        <b style="color: black">Amount you want to buy</b>\n      </p>\n    </ion-list>\n\n    <ion-item>\n      <!-- <ion-label stacked> Amount you want to buy</ion-label> -->\n      <ion-input\n        type="number"\n        [(ngModel)]="list_items.amount"\n        placeholder="Amount (GHC)"\n        formControlName="amount"\n        name="amount"\n        [class.invalid]="!listForm.controls.amount.valid && (listForm.controls.amount.dirty || submitAttempt)"\n      ></ion-input>\n    </ion-item>\n\n    <p>\n      <button\n        ion-button\n        color="secondary"\n        class="button button-dark customBtn"\n        [disabled]="!this.listForm.valid"\n        (tap)="addtoList()"\n      >\n        Add to list\n      </button>\n    </p>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/createlist/createlist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */]])
    ], CreatelistPage);
    return CreatelistPage;
}());

//# sourceMappingURL=createlist.js.map

/***/ })

});
//# sourceMappingURL=31.js.map