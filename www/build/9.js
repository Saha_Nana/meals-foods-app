webpackJsonp([9],{

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileEditPageModule", function() { return ProfileEditPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_edit__ = __webpack_require__(359);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileEditPageModule = /** @class */ (function () {
    function ProfileEditPageModule() {
    }
    ProfileEditPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_edit__["a" /* ProfileEditPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_edit__["a" /* ProfileEditPage */]),
            ],
        })
    ], ProfileEditPageModule);
    return ProfileEditPageModule;
}());

//# sourceMappingURL=profile-edit.module.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileEditPage = /** @class */ (function () {
    function ProfileEditPage(alertCtrl, apis, loadingCtrl, _form, navCtrl, navParams) {
        // this.user_details = this.navParams.get("user_details")
        this.alertCtrl = alertCtrl;
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this._form = _form;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profile = { first_name: '', last_name: '', mobile_number: '', email: '' };
        this.from_login = this.navParams.get("user_details");
        console.log("from_login " + JSON.stringify(this.from_login));
        this.user_details = JSON.parse(this.from_login);
        this.user_id = this.user_details[0].id;
        // this.person_id = this.user_details[0].id
        console.log("USER ID 1 " + this.user_id);
        console.log("user_details" + this.user_details);
        // console.log("PERSON ID 1 " + this.person_id )
        this.updateProfile = this._form.group({
            "first_name": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "last_name": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "mobile_number": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "email": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
        if (this.navParams.get('user_details')) {
            this.profile.first_name = this.user_details[0].first_name;
            this.profile.last_name = this.user_details[0].last_name;
            this.profile.mobile_number = this.user_details[0].mobile_number;
            this.profile.email = this.user_details[0].email;
        }
    }
    ProfileEditPage.prototype.editProfile = function () {
        var _this = this;
        this.loginVal = JSON.stringify(this.updateProfile.value);
        this.jsonBody = JSON.parse(this.loginVal);
        this.user_id = JSON.parse(this.user_details[0].id);
        console.log("loginVal " + this.loginVal);
        console.log("jsonBody " + this.jsonBody);
        console.log("USER ID 2 " + this.user_id);
        this.params = {
            "id": this.user_id,
            "first_name": this.jsonBody.first_name,
            "last_name": this.jsonBody.last_name,
            "mobile_number": this.jsonBody.mobile_number,
            "email": this.jsonBody.email
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.update_profile(this.params).then(function (result) {
            // this.body1 = result;
            // this.retrieved = this.body1
            // this.retrieved = JSON.stringify(this.body1)
            console.log("THIS IS THE RESULT" + result);
            console.log("result" + JSON.stringify(result));
            var resp_code = result["resp_code"];
            var resp_desc = result["resp_desc"];
            console.log(resp_code);
            console.log(resp_desc);
            _this.messageList = resp_desc;
            _this.api_code = resp_code;
            console.log("retrieved" + _this.retrieved);
            var alert = _this.alertCtrl.create({
                title: "Profile Updated",
                subTitle: _this.messageList,
                buttons: [
                    {
                        text: 'OK', handler: function () {
                            // this.navCtrl.setRoot("HomePage", { "customer_details": this.from_login });
                            _this.navCtrl.setRoot("ProfilePage", { "customer_details": _this.from_login });
                        }
                    }
                ]
            });
            alert.present();
        });
        loader.dismiss();
    };
    ProfileEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile-edit',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/profile-edit/profile-edit.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Edit Your Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding >\n    <form [formGroup]="updateProfile">   \n  <br>\n \n\n  <ion-item>\n    <ion-label stacked>First Name</ion-label>\n    <ion-input type="text" placeholder="first name" formControlName="first_name" name="first_name" [(ngModel)]="profile.first_name" [class.invalid]="!updateProfile.controls.first_name.valid && (updateProfile.controls.first_name.dirty || submitAttempt)"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>Last Names</ion-label>\n    <ion-input type="text" placeholder="last name" formControlName="last_name" name="last_name" [(ngModel)]="profile.last_name" [class.invalid]="!updateProfile.controls.last_name.valid && (updateProfile.controls.last_name.dirty || submitAttempt)"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>Mobile Number</ion-label>\n    <ion-input type="text" placeholder="mobile number" formControlName="mobile_number" name="mobile_number" [(ngModel)]="profile.mobile_number" [class.invalid]="!updateProfile.controls.mobile_number.valid && (updateProfile.controls.mobile_number.dirty || submitAttempt)"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label stacked>Email</ion-label>\n    <ion-input type="text" placeholder="email" formControlName="email" name="email" [(ngModel)]="profile.email" [class.invalid]="!updateProfile.controls.email.valid && (updateProfile.controls.email.dirty || submitAttempt)"></ion-input>\n  </ion-item>\n\n \n  \n  </form> \n</ion-content>\n\n<ion-footer>\n    <p>\n        <button ion-button color="primary" class="button button-dark customBtn" [disabled]="!this.updateProfile.valid" (tap)="editProfile()"> Edit Profile</button>\n      </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/profile-edit/profile-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ProfileEditPage);
    return ProfileEditPage;
}());

//# sourceMappingURL=profile-edit.js.map

/***/ })

});
//# sourceMappingURL=9.js.map