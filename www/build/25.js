webpackJsonp([25],{

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodOrderDetailsPageModule", function() { return FoodOrderDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_order_details__ = __webpack_require__(344);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodOrderDetailsPageModule = /** @class */ (function () {
    function FoodOrderDetailsPageModule() {
    }
    FoodOrderDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_order_details__["a" /* FoodOrderDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_order_details__["a" /* FoodOrderDetailsPage */]),
            ],
        })
    ], FoodOrderDetailsPageModule);
    return FoodOrderDetailsPageModule;
}());

//# sourceMappingURL=food-order-details.module.js.map

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodOrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FoodOrderDetailsPage = /** @class */ (function () {
    function FoodOrderDetailsPage(navCtrl, navParams, apis, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.orderJson = this.navParams.get('value');
        this.user_details = this.navParams.get("customer_details");
        console.log("ORDER ID IN MODAL " + this.orderJson);
        this.params = {
            "order_id": this.orderJson
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.apis.food_order_details(this.params).then(function (result) {
            _this.list = Array.of(result);
            _this.orderJson = JSON.stringify(result);
            _this.order_id = result['order_id'];
            _this.orderlist = result['order_details'];
            _this.total_price = _this.orderlist[0].total_price;
            console.log('LIST ' + _this.list);
            console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
            console.log('LETS SEE THE ORDER ID IN PROCESS ORDER ' + _this.order_id);
            console.log('TOTAL PRICE' + _this.total_price);
            loader.dismiss();
        });
    }
    FoodOrderDetailsPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    FoodOrderDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-order-details',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-order-details/food-order-details.html"*/'<ion-header>\n    <ion-navbar>\n  \n      <ion-title> <b>Food Order Details </b> </ion-title>\n    </ion-navbar>\n  </ion-header>\n\n\n<ion-content >\n\n  \n      <ion-list *ngFor="let q of list">\n  \n        <ion-list *ngFor=" let option of q.order_details">\n  \n          <ion-item no-lines>\n            <ion-thumbnail item-left>\n           <img *ngIf="option" [src]="option.food_logo_url"  />\n            </ion-thumbnail>\n         \n            <p text-wrap> Product name:{{ option.food_name}}</p> \n            <p text-wrap> Price: GHC {{ option.price}}</p>\n            \n              <!-- <p text-wrap > Special Requests: {{ option.requests}}</p>\n                <p text-wrap> Specific Delivery Location: {{ option.new_location}}</p> -->\n            \n          </ion-item>\n  \n        </ion-list>\n      </ion-list>\n      <br>\n     <p class="pad" >TOTAL AMOUNT: {{total_price}}</p>\n  </ion-content>\n\n  <ion-footer >\n      <p>\n        <button ion-button color="dark" class="button button-dark customBtn" (tap)="home()"> Order Again? </button>\n      </p>\n    \n    </ion-footer>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-order-details/food-order-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FoodOrderDetailsPage);
    return FoodOrderDetailsPage;
}());

//# sourceMappingURL=food-order-details.js.map

/***/ })

});
//# sourceMappingURL=25.js.map