webpackJsonp([12],{

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderHistoryPageModule", function() { return OrderHistoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_history__ = __webpack_require__(357);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OrderHistoryPageModule = /** @class */ (function () {
    function OrderHistoryPageModule() {
    }
    OrderHistoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order_history__["a" /* OrderHistoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__order_history__["a" /* OrderHistoryPage */]),
            ],
        })
    ], OrderHistoryPageModule);
    return OrderHistoryPageModule;
}());

//# sourceMappingURL=order-history.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrderHistoryPage = /** @class */ (function () {
    function OrderHistoryPage(toastCtrl, navCtrl, modalCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.order_history = "Foodstuffs";
        this.user_details = this.navParams.get("customer_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
        }
        else {
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            console.log("USer DETAILS " + this.user_details);
            console.log("USer ID " + this.user_id);
            this.params = {
                "user_id": this.user_id
            };
            var loader_1 = this.loadingCtrl.create({
                content: "Please wait ...",
            });
            loader_1.present();
            this.apis.order_history(this.params).then(function (result) {
                console.log(result);
                if (result["resp_code"] == "112") {
                    _this.toastCtrl.create({
                        message: "No Orders Available yet",
                        duration: 3000
                    }).present();
                    loader_1.dismiss();
                }
                else {
                    _this.orderJson = JSON.stringify(result);
                    _this.order_id = result['order_id'];
                    console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
                    _this.list = JSON.parse(_this.orderJson);
                    _this.delivered = result[0].delivered;
                    console.log('LETS SEE LIST delivered ' + _this.delivered);
                    loader_1.dismiss();
                }
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ['OK']
                });
                alert.present();
                loader_1.dismiss();
            });
            this.apis.food_order_history(this.params).then(function (result) {
                console.log(result);
                if (result["resp_code"] == "112") {
                    _this.toastCtrl.create({
                        message: "No Orders Available yet",
                        duration: 3000
                    }).present();
                    loader_1.dismiss();
                }
                else {
                    _this.orderJson = JSON.stringify(result);
                    _this.order_id = result['order_id'];
                    console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
                    _this.list2 = JSON.parse(_this.orderJson);
                    _this.delivered = result[0].delivered;
                    console.log('LETS SEE LIST delivered ' + _this.delivered);
                    loader_1.dismiss();
                }
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ['OK']
                });
                alert.present();
                loader_1.dismiss();
            });
            //CATRERIN HISTORY
            this.apis.catering_order_history(this.params).then(function (result) {
                console.log(result);
                if (result["resp_code"] == "112") {
                    _this.toastCtrl.create({
                        message: "No Orders Available yet",
                        duration: 3000
                    }).present();
                    loader_1.dismiss();
                }
                else {
                    _this.orderJson = JSON.stringify(result);
                    _this.order_id = result['order_id'];
                    console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
                    _this.list3 = JSON.parse(_this.orderJson);
                    _this.delivered = result[0].delivered;
                    console.log('LETS SEE LIST delivered ' + _this.delivered);
                    loader_1.dismiss();
                }
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ['OK']
                });
                alert.present();
                loader_1.dismiss();
            });
            //CATRERIN HISTORY END
        }
    }
    OrderHistoryPage.prototype.details = function (q) {
        var select = q.order_id;
        console.log("LETS SEE ID SELECTED " + select);
        this.navCtrl.push("OrderDetailsPage", { value: select, customer_details: this.user_details });
    };
    OrderHistoryPage.prototype.details2 = function (q) {
        var select = q.order_id;
        console.log("LETS SEE ID SELECTED " + select);
        this.navCtrl.push("FoodOrderDetailsPage", { value: select, customer_details: this.user_details });
    };
    OrderHistoryPage.prototype.details3 = function (q) {
        var select = q.order_id;
        console.log("LETS SEE ID SELECTED " + select);
        this.navCtrl.push("CateringOrderDetailsPage", { value: select, customer_details: this.user_details });
    };
    OrderHistoryPage.prototype.track = function (q) {
        var select = q.order_id;
        console.log("LETS SEE ID SELECTED " + select);
        this.navCtrl.push("TrackingPage", { value: select, customer_details: this.user_details });
    };
    OrderHistoryPage.prototype.home = function () {
        this.navCtrl.setRoot("HomePage", { customer_details: this.user_details });
    };
    OrderHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order-history',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/order-history/order-history.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Order History</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <ion-segment [(ngModel)]="order_history" overflow-scroll="true">\n    <ion-segment-button value="Foodstuffs" text-wrap>\n      Foodstuffs History\n    </ion-segment-button>\n\n    <ion-segment-button class="segment-button" value="Meals" text-wrap>\n      Meals Order History\n    </ion-segment-button>\n\n    <ion-segment-button class="segment-button" value="Catering" text-wrap>\n      Catering Order History\n    </ion-segment-button>\n  </ion-segment>\n\n  <div [ngSwitch]="order_history">\n    <ion-list *ngSwitchCase="\'Meals\'" overflow-scroll="true">\n      <ion-card>\n        <div\n          class="center1"\n          *ngIf="list?.length == 0 || body?.length == null"\n          text-wrap\n        >\n          <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n          <p style="color: #757575; text-align: center">\n            Order History Not Available\n          </p>\n        </div>\n\n        <ion-list *ngFor="let q of list">\n          <ion-item>\n            <p>Order ID: {{q.order_id}}</p>\n            <p>Order Date: {{q.order_date | date: \'medium\'}}</p>\n            <p *ngIf="q.delivered == true">Order Status: Delivered</p>\n            <p *ngIf="q.delivered == false">\n              Order Status: Preparing For delivery\n            </p>\n            <p>\n              <button\n                ion-button\n                color="secondary"\n                class="button button-dark customBtn"\n                (tap)="details(q)"\n              >\n                View Details\n              </button>\n            </p>\n            <p>\n              <button\n                ion-button\n                color="primary"\n                class="button button-dark customBtn"\n                (tap)="track(q)"\n              >\n                Track Order\n              </button>\n            </p>\n          </ion-item>\n        </ion-list>\n      </ion-card>\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'Foodstuffs\'" overflow-scroll="true">\n      <ion-card>\n        <div\n          class="center1"\n          *ngIf="list2?.length == 0 || body?.length == null"\n          text-wrap\n        >\n          <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n          <p style="color: #757575; text-align: center">\n            Order History Not Available\n          </p>\n        </div>\n\n        <ion-list *ngFor="let q of list2">\n          <ion-item>\n            <p>Order ID: {{q.order_id}}</p>\n            <p>Order Date: {{q.order_date | date: \'medium\'}}</p>\n            <!-- <p> Total Amount: {{q.total_price}}</p> -->\n            <!-- <p *ngIf="q.delivered == true">Order Status: Delivered</p>\n             <p *ngIf="q.delivered == false">Order Status: Preparing For delivery</p> -->\n            <p>\n              <button\n                ion-button\n                color="secondary"\n                class="button button-dark customBtn"\n                (tap)="details2(q)"\n              >\n                View Details\n              </button>\n            </p>\n            <!-- <p> <button ion-button color="primary" class="button button-dark customBtn"  (tap)="track(q)"> Track Order</button></p>\n          -->\n          </ion-item>\n        </ion-list>\n      </ion-card>\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'Catering\'" overflow-scroll="true">\n      <ion-card>\n        <div\n          class="center1"\n          *ngIf="list2?.length == 0 || body?.length == null"\n          text-wrap\n        >\n          <img src="assets/imgs/notavail.svg" height="50px" width="70px" />\n          <p style="color: #757575; text-align: center">\n            Order History Not Available\n          </p>\n        </div>\n\n        <ion-list *ngFor="let q of list3">\n          <ion-item>\n            <p>Order ID: {{q.order_id}}</p>\n            <p>Order Date: {{q.order_date | date: \'medium\'}}</p>\n            <!-- <p> Total Amount: {{q.total_price}}</p> -->\n            <!-- <p *ngIf="q.delivered == true">Order Status: Delivered</p>\n             <p *ngIf="q.delivered == false">Order Status: Preparing For delivery</p> -->\n            <p>\n              <button\n                ion-button\n                color="secondary"\n                class="button button-dark customBtn"\n                (tap)="details3(q)"\n              >\n                View Details\n              </button>\n            </p>\n            <!-- <p> <button ion-button color="primary" class="button button-dark customBtn"  (tap)="track(q)"> Track Order</button></p>\n          -->\n          </ion-item>\n        </ion-list>\n      </ion-card>\n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <p>\n    <button\n      ion-button\n      color="dark"\n      class="button button-dark customBtn"\n      (tap)="home()"\n    >\n      Continue Order\n    </button>\n  </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/order-history/order-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["b" /* CartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OrderHistoryPage);
    return OrderHistoryPage;
}());

//# sourceMappingURL=order-history.js.map

/***/ })

});
//# sourceMappingURL=12.js.map