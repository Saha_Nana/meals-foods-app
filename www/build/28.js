webpackJsonp([28],{

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FoodCheckoutPageModule", function() { return FoodCheckoutPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__food_checkout__ = __webpack_require__(341);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FoodCheckoutPageModule = /** @class */ (function () {
    function FoodCheckoutPageModule() {
    }
    FoodCheckoutPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__food_checkout__["a" /* FoodCheckoutPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__food_checkout__["a" /* FoodCheckoutPage */]),
            ],
        })
    ], FoodCheckoutPageModule);
    return FoodCheckoutPageModule;
}());

//# sourceMappingURL=food-checkout.module.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FoodCheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FoodCheckoutPage = /** @class */ (function () {
    function FoodCheckoutPage(navCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.fakeUsers = new Array(1);
        this.user_details = this.navParams.get("user_details");
        this.order_id = this.navParams.get("order_id");
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        this.mobile_number = this.jsonBody[0].mobile_number;
        console.log("USer DETAILS " + this.user_details);
        console.log("USer ID " + this.user_id);
        console.log("mobile_number " + this.mobile_number);
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        this.cartList = cartServ.getAllCartfoodItems();
        console.log("CARTLIST ITEMS " + JSON.stringify(this.cartList));
        this.params = {
            "user_id": this.user_id
        };
        //Retrieve Time SLOTS
        this.apis.retrieve_time_slot().then(function (result) {
            console.log(result);
            _this.time = result;
            console.log("time SLOTS" + _this.time);
            console.log("time SLOTS STRING" + JSON.stringify(_this.time));
        });
        //Retrieve Loyalty Points
        this.apis.retrieve_loyalty_points(this.params).then(function (result) {
            console.log(result);
            _this.points = result[0].points;
            _this.redeemable_amount = result[0].redeemable_amount;
            console.log("points,redeemable_amount " + _this.points, _this.redeemable_amount);
        });
        //END Retrieve Loyalty Points
        this.apis.retrieve_address_details(this.params).then(function (result) {
            _this.body2 = result;
            _this.address_details = JSON.stringify(_this.body2);
            console.log("Lets see all the Adresss details as body " + _this.body2);
            console.log("Lets see all the Adresss details " + _this.address_details);
        });
        this.apis.retrieve_delivery_charges().then(function (result) {
            _this.body = result;
            console.log(_this.body);
            console.log(JSON.stringify(_this.body));
            console.log("Information", _this.body);
            _this.service_fee = _this.body[0].service_fee;
            _this.delivery_fee = _this.body[0].delivery_fee;
            console.log("------------SERV FEE _+ DELIVE-------------------------------------------");
            console.log("SERVICE FEE" + _this.service_fee);
            console.log("DEL FEE " + _this.delivery_fee);
            console.log("CARTLIST ITEMS " + JSON.stringify(_this.cartList));
            if (_this.points > 9999) {
                _this.total_amount_plus_delivery = (_this.getTotal() - _this.redeemable_amount) + (_this.getTotal() * _this.service_fee) + _this.delivery_fee;
                console.log("TOTAL AMOUNT PLUS WITH REDEEMABLE DELIVERY" + _this.total_amount_plus_delivery);
            }
            else {
                _this.total_amount_plus_delivery = (_this.getTotal()) + (_this.getTotal() * _this.service_fee) + _this.delivery_fee;
                console.log("TOTAL AMOUNT MINUS REDDEEMA PLUS DELIVERY" + _this.total_amount_plus_delivery);
            }
            // this.total_amount_plus_delivery = (this.getTotal() - this.redeemable_amount) + (this.getTotal()* this.service_fee ) + this.delivery_fee
            // console.log("TOTAL AMOUNT PLUS DELIVERY" + this.total_amount_plus_delivery)
        });
        loader.dismiss();
    }
    FoodCheckoutPage.prototype.getTotal = function () {
        this.total_amt = this.cartServ.getGrandTotal();
        return this.cartServ.getGrandTotal();
        // this.total_amount_plus_delivery = getTotal() + (getTotal()*0.10) + 15 
    };
    FoodCheckoutPage.prototype.updateCredit = function () {
        console.log('Cucumbers new state:' + this.card);
    };
    FoodCheckoutPage.prototype.checkout = function () {
        var _this = this;
        var prod_details = this.cartList;
        var orders = {};
        var new_list = [];
        for (var x in prod_details) {
            new_list.push({ 'food_id': prod_details[x]['food_id'], 'quantity': prod_details[x]['quantity'], 'price': prod_details[x]['price'] });
        }
        orders = new_list;
        console.log(new_list);
        console.log(JSON.stringify(orders));
        console.log("TOTAL AMOUNT PLUS DELIVERY" + this.total_amount_plus_delivery);
        this.params = {
            "user_id": this.user_id,
            "orders": orders,
            "total_price": this.total_amount_plus_delivery
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        console.log("PARAMS + ");
        console.log(this.params);
        this.apis.process_food_order(this.params).then(function (result) {
            console.log('LETS RESULTS ' + result);
            console.log(result);
            var body1 = result;
            _this.orderJson = JSON.stringify(body1);
            _this.order_id = body1['order_id'];
            console.log('LETS SEE THE PROCESS ORDER ' + _this.orderJson);
            console.log('LETS SEE THE ORDER ID IN PROCESS ORDER ' + _this.order_id);
            loader.dismiss();
            _this.navCtrl.push("FoodPaymentPage", { order_id: _this.order_id, user_details: _this.user_details, requests: _this.SpecialRequest, new_location: _this.NewLocation, total_amount_plus_delivery: _this.total_amount_plus_delivery });
        });
        //params for special requests
        console.log('SpecialRequest IS' + this.SpecialRequest);
        console.log('NewLocation IS' + this.NewLocation);
        console.log('order_id IS' + this.order_id);
        this.params2 = {
            "order_id": this.order_id,
            "requests": this.SpecialRequest,
            "new_location": this.NewLocation
        };
    };
    FoodCheckoutPage.prototype.edit = function (item) {
        this.navCtrl.push("AddressPage", { addressDetails: this.body2, user_details: this.user_details, from_checkout: "1" });
    };
    FoodCheckoutPage.prototype.not = function () {
        var alert = this.alertCtrl.create({
            title: "Oopps!",
            subTitle: "Feature not available",
            buttons: ['OK']
        });
        alert.present();
    };
    FoodCheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-food-checkout',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/food-checkout/food-checkout.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Foodstuffs Checkout</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-item *ngFor="let item of body2">\n    <ion-row class="container">\n      <ion-col col-9>\n        <h2 style="color: #781318">Default Delivery Address</h2>\n        <p style="font-size: 13px !important" text-wrap>\n          {{item.address_name}} <br />{{item.landmark}} <br />{{mobile_number}}\n        </p>\n      </ion-col>\n      <ion-col col-3>\n        <button ion-button color="secondary" outline (tap)="edit(item)">\n          Edit\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n\n  <ion-list *ngIf="!body2">\n    <ion-list-header\n      *ngFor="let fake of fakeUsers; let i = index"\n      no-lines\n      no-padding\n      class="fakeItem"\n    >\n      <ion-row class="container">\n        <ion-col col-9>\n          <h2></h2>\n          <p style="font-size: 13px !important" text-wrap>\n            <br />\n            <br />\n          </p>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button color="secondary" outline>Edit</button>\n        </ion-col>\n      </ion-row>\n    </ion-list-header>\n  </ion-list>\n\n  <ion-item no-lines *ngFor="let item of cartList">\n    <ion-row class="container">\n      <ion-col col-9 text-wrap>\n        <h2 text-wrap>{{ item.food_name}}</h2>\n        <p style="font-size: 13px !important" *ngIf="item.quantity > 0">\n          Quantity x {{ item.quantity}}\n        </p>\n      </ion-col>\n      <ion-col col-3 class="move-down">\n        <p text-wrap>Ghc {{ item.price | number:\'1.2-2\'}}</p>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n\n  <ion-item>\n    <ion-row>\n      <ion-col col-8>\n        <p text-wrap>Subtotal</p>\n      </ion-col>\n      <ion-col col-4>\n        <p text-wrap>Ghc {{getTotal() | number:\'1.2-2\'}}</p>\n      </ion-col>\n\n      <ion-col col-8>\n        <p text-wrap>Deliver Fee</p>\n      </ion-col>\n      <ion-col col-4>\n        <p text-wrap>\n          Ghc {{(getTotal()* service_fee ) + delivery_fee | number:\'1.2-2\'}}\n        </p>\n      </ion-col>\n\n      <!-- SHOW REDEEEMABLE POINTS -->\n      <div *ngIf="points > 9999">\n        <ion-col col-8>\n          <p text-wrap>Loyalty Point Redeemed</p>\n        </ion-col>\n        <ion-col col-4>\n          <p text-wrap>( Ghc {{redeemable_amount | number:\'1.2-2\'}} )</p>\n          <p text-wrap>( Ghc 50.00 )</p>\n        </ion-col>\n\n        <ion-col col-8>\n          <p text-wrap>Total Amount</p>\n        </ion-col>\n        <ion-col col-4>\n          <p text-wrap>\n            Ghc {{(getTotal() - 50) + (getTotal()* service_fee ) + delivery_fee\n            | number:\'1.2-2\'}}\n          </p>\n        </ion-col>\n      </div>\n\n      <!-- DONT SHOW REDEEEMABLE POINTS -->\n      <div *ngIf="points < 10000">\n        <ion-col col-8>\n          <p text-wrap>Total Amount</p>\n        </ion-col>\n        <ion-col col-4>\n          <p text-wrap>\n            Ghc {{(getTotal()) + (getTotal()* service_fee ) + delivery_fee |\n            number:\'1.2-2\'}}\n          </p>\n        </ion-col>\n      </div>\n    </ion-row>\n  </ion-item>\n\n  <ion-item no-lines>\n    <h2>Special Request (optional)</h2>\n  </ion-item>\n  <ion-item>\n    <ion-input\n      type="text"\n      placeholder="Type here..."\n      [(ngModel)]="SpecialRequest"\n    ></ion-input>\n  </ion-item>\n\n  <!-- <ion-item no-lines>\n    <h2>Discount Voucher</h2>\n  </ion-item>\n\n  <ion-item>\n    <ion-input type="text" placeholder="Voucher Code"></ion-input>\n  </ion-item> -->\n\n  <ion-item no-lines>\n    <h2 style="color: #781318">\n      <b>Specify Delivery Location</b>\n    </h2>\n  </ion-item>\n\n  <ion-list radio-group [(ngModel)]="yesorno">\n    <ion-item no-lines>\n      <ion-label> Default Location</ion-label>\n      <!-- <ion-radio checked = "true" value="N" checked></ion-radio> -->\n\n      <ion-radio checked="true" value="N"></ion-radio>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-label> Change Delivery Location</ion-label>\n      <ion-radio value="Y"></ion-radio>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-label> Pickup At Shop</ion-label>\n      <ion-radio value="P"></ion-radio>\n    </ion-item>\n  </ion-list>\n\n  <ion-list *ngIf="yesorno == \'Y\'">\n    <ion-item no-lines>\n      <h2>New Delivery Location</h2>\n    </ion-item>\n\n    <ion-item>\n      <ion-input\n        type="text"\n        placeholder="Enter Your new Delivery Location"\n        [(ngModel)]="NewLocation"\n      ></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <ion-item no-lines>\n    <h2 style="color: #781318">\n      <b>Choose Delivery Time</b>\n    </h2>\n  </ion-item>\n\n  <ion-list radio-group *ngFor="let item of time" [(ngModel)]="time_selected">\n    <ion-item no-lines>\n      <ion-label> {{item.time | date: \'medium\' }}</ion-label>\n      <p>rttd</p>\n\n      <ion-radio [value]="item.time"></ion-radio>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer>\n  <p>\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      (tap)="checkout()"\n    >\n      Proceed to Pay\n    </button>\n  </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/food-checkout/food-checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["d" /* FoodCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FoodCheckoutPage);
    return FoodCheckoutPage;
}());

//# sourceMappingURL=food-checkout.js.map

/***/ })

});
//# sourceMappingURL=28.js.map