webpackJsonp([43],{

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address__ = __webpack_require__(326);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddressPageModule = /** @class */ (function () {
    function AddressPageModule() {
    }
    AddressPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */]),
            ],
        })
    ], AddressPageModule);
    return AddressPageModule;
}());

//# sourceMappingURL=address.module.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddressPage = /** @class */ (function () {
    function AddressPage(toastCtrl, apis, _form, loadingCtrl, alertCtrl, navCtrl, navParams, http) {
        this.toastCtrl = toastCtrl;
        this.apis = apis;
        this._form = _form;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.address = { address_name: '', landmark: '', directions: '', additional_instructions: '' };
        this.user_details = this.navParams.get("user_details");
        this.from_checkout = this.navParams.get("from_checkout");
        this.order_id = this.navParams.get("order_id");
        console.log("FROM CHECKOUT" + this.from_checkout);
        console.log("ORDER ID" + this.order_id);
        this.body = this.user_details;
        this.jsonBody = JSON.parse(this.body);
        this.user_id = this.jsonBody[0].id;
        console.log("USer DETAILS " + this.user_details);
        console.log("USer ID " + this.user_id);
        this.addForm = this._form.group({
            "address_name": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "landmark": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
            "directions": [""],
            "additional_instructions": [""]
        });
        this.addressdata = this.navParams.get('addressDetails');
        console.log(this.addressdata);
        if (this.navParams.get('addressDetails')) {
            this.address.address_name = this.addressdata[0].address_name;
            this.address.landmark = this.addressdata[0].landmark;
            this.address.directions = this.addressdata[0].directions;
            this.address.additional_instructions = this.addressdata[0].additional_instructions;
        }
    }
    AddressPage.prototype.save_address = function () {
        var _this = this;
        this.loginVal = JSON.stringify(this.addForm.value);
        this.jsonBody = JSON.parse(this.loginVal);
        this.params = {
            "user_id": this.user_id,
            "address_name": this.jsonBody.address_name,
            "landmark": this.jsonBody.landmark,
            "directions": this.jsonBody.directions,
            "additional_instructions": this.jsonBody.additional_instructions
        };
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
        });
        loader.present();
        if (this.navParams.get('addressDetails')) {
            console.log("--------UPDATING ADDRESS-----");
            this.apis.update_address(this.params).then(function (result) {
                _this.body1 = result;
                _this.retrieved = _this.body1;
                _this.retrieved = JSON.stringify(_this.body1);
            });
        }
        else {
            console.log("--------CREATING ADDRESS-----");
            this.apis.save_address(this.params).then(function (result) {
                _this.body1 = result;
                _this.retrieved = _this.body1;
                _this.retrieved = JSON.stringify(_this.body1);
            });
        }
        loader.dismiss();
        this.navCtrl.push("FoodCheckoutPage", { user_details: this.user_details, order_id: this.order_id });
        // if (this.from_checkout == "1"){
        //   this.navCtrl.push("FoodCheckoutPage", { user_details: this.user_details })
        // }
        // else{
        //   this.navCtrl.push("CheckoutPage", { user_details: this.user_details })
        // }
    };
    AddressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-address',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/address/address.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Enter Your Address</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <form [formGroup]="addForm">\n    <br />\n    <ion-item>\n      <ion-label stacked>Residential Address</ion-label>\n      <ion-input\n        type="text"\n        placeholder=""\n        formControlName="address_name"\n        name="address_name"\n        [(ngModel)]="address.address_name"\n        [class.invalid]="!addForm.controls.address_name.valid && (addForm.controls.address_name.dirty || submitAttempt)"\n      ></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked> Digital Address</ion-label>\n      <ion-input\n        type="text"\n        placeholder="Digital Address (e.g. GK-23F-5674)\n"\n        formControlName="landmark"\n        name="landmark"\n        [(ngModel)]="address.landmark"\n        [class.invalid]="!addForm.controls.landmark.valid && (addForm.controls.landmark.dirty || submitAttempt)"\n      ></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked>Directions</ion-label>\n      <ion-input\n        type="text"\n        placeholder="Directions"\n        formControlName="directions"\n        name="directions"\n        [(ngModel)]="address.directions"\n        [class.invalid]="!addForm.controls.directions.valid && (addForm.controls.directions.dirty || submitAttempt)"\n      ></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked> Additional Direction (Optional)</ion-label>\n      <ion-input\n        type="text"\n        placeholder="please include any landmarks"\n        formControlName="additional_instructions"\n        name="additional_instructions"\n        [(ngModel)]="address.additional_instructions"\n        [class.invalid]="!addForm.controls.additional_instructions.valid && (addForm.controls.additional_instructions.dirty || submitAttempt)"\n      ></ion-input>\n    </ion-item>\n  </form>\n</ion-content>\n\n<ion-footer>\n  <p>\n    <button\n      ion-button\n      color="secondary"\n      class="button button-dark customBtn"\n      [disabled]="!this.addForm.valid"\n      (tap)="save_address()"\n    >\n      Save Address\n    </button>\n  </p>\n</ion-footer>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/address/address.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */]])
    ], AddressPage);
    return AddressPage;
}());

//# sourceMappingURL=address.js.map

/***/ })

});
//# sourceMappingURL=43.js.map