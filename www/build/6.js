webpackJsonp([6],{

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetcodePageModule", function() { return ResetcodePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__resetcode__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ResetcodePageModule = /** @class */ (function () {
    function ResetcodePageModule() {
    }
    ResetcodePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__resetcode__["a" /* ResetcodePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__resetcode__["a" /* ResetcodePage */]),
            ],
        })
    ], ResetcodePageModule);
    return ResetcodePageModule;
}());

//# sourceMappingURL=resetcode.module.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetcodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResetcodePage = /** @class */ (function () {
    function ResetcodePage(toastCtrl, apis, _form, loadingCtrl, alertCtrl, navCtrl, navParams, http) {
        this.toastCtrl = toastCtrl;
        this.apis = apis;
        this._form = _form;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.mobile_number = this.navParams.get('mobile_number');
        console.log("mobile_number is " + this.mobile_number);
        this.resetPasswordForm = this._form.group({
            "resetcode": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
        });
    }
    ResetcodePage.prototype.reset = function () {
        //send phone number to api in order to recieve sms code and proceed to the next page.
        var _this = this;
        this.params = {
            "mobile_number": this.mobile_number,
            "reset_token": this.resetPasswordForm.value.resetcode
        };
        console.log("this.params = " + JSON.stringify(this.params));
        var loader = this.loadingCtrl.create({
            content: "Please wait ..."
        });
        loader.present();
        this.apis.reset_password_get_smscode(this.params).then(function (result) {
            if (result) {
                console.log("THIS IS THE RESULT" + result);
                console.log("THIS IS THE RESULT" + JSON.stringify(result));
                var jsonBody = result;
                console.log(jsonBody);
                // jsonBody = JSON.parse(jsonBody);
                // console.log(jsonBody)
                var desc = jsonBody["resp_desc"];
                var code = jsonBody["resp_code"];
                var reset_sms_code = jsonBody["db_reset_token"];
                console.log(desc);
                console.log(code);
                console.log(reset_sms_code);
                loader.dismiss();
                if (code == "000") {
                    _this.navCtrl.push("ChangepasswordPage", { mobile_number: _this.mobile_number, reset_sms_code: reset_sms_code });
                }
                else {
                    _this.showalertmessage("M&F", desc);
                }
            }
        }, function (err) {
            loader.dismiss();
            _this.toastCtrl.create({
                message: "Could not complete this request successfully.",
                duration: 5000
            }).present();
            console.log(err);
        });
    };
    ResetcodePage.prototype.showmessage = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ResetcodePage.prototype.showalertmessage = function (titlemsg, mainmsg) {
        var alert = this.alertCtrl.create({
            title: titlemsg,
            subTitle: mainmsg,
            buttons: ['OK']
        });
        alert.present();
    };
    ResetcodePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-resetcode',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/resetcode/resetcode.html"*/'<ion-header>\n\n    <ion-navbar>\n      <ion-title>Enter reset code</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n\n<ion-content overflow-scroll=\'true\' class="background">\n    <br/>\n    <br/>\n    <br/>\n    <br/>\n    <br/>\n    <form [formGroup]="resetPasswordForm">\n  \n      <ion-list class="main-view">\n        <br/>\n        <br/>\n        <br/>\n        <br/>\n        <ion-item>\n          <ion-input type="text" placeholder="Enter Reset code sent via SMS" formControlName="resetcode" name="resetcode" [class.invalid]="!resetPasswordForm.controls.resetcode.valid && (resetPasswordForm.controls.resetcode.dirty || submitAttempt)"></ion-input>\n         \n          <ion-label> </ion-label>\n        </ion-item>\n        <br/>\n  \n     \n  \n        <br/>\n        <p>\n          <button ion-button color="kaya" class="button button-dark customBtn" [disabled]="!this.resetPasswordForm.valid" (tap)="reset()">\n            Proceed</button>\n        </p>\n        \n  \n       \n      </ion-list>\n  \n  \n  \n    </form>\n  </ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/resetcode/resetcode.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]])
    ], ResetcodePage);
    return ResetcodePage;
}());

//# sourceMappingURL=resetcode.js.map

/***/ })

});
//# sourceMappingURL=6.js.map