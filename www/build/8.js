webpackJsonp([8],{

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfilePage = /** @class */ (function () {
    function ProfilePage(apis, loadingCtrl, menuCtrl, navCtrl, navParams) {
        // this.storage.get('value').then((value) => {
        //   this.user_details = value
        //   console.log("this.user_details " +  JSON.stringify(this.user_details))
        //   this.user_type = this.user_details[0].user_type
        // });
        this.apis = apis;
        this.loadingCtrl = loadingCtrl;
        this.menuCtrl = menuCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.from_login = [];
        this.from_login = this.navParams.get("customer_details");
        console.log("from_login " + JSON.stringify(this.from_login));
        this.user_details = JSON.parse(this.from_login);
        // console.log("this.user_details " +  JSON.stringify(this.user_details))
        // console.log("from_login " +  this.from_login)
        // console.log("user_details " +  this.user_details)
        //this.username = this.user_details[0].username
        // this.reg_id = this.user_details[0].reg_id
        // this.user_type = this.user_details[0].user_type
        // console.log("USer DETAILS " + JSON.stringify(this.user_details))
        // console.log("Username " + JSON.stringify(this.username))
        // console.log("ID " + JSON.stringify(this.reg_id))
    }
    ProfilePage.prototype.edit = function (item) {
        this.navCtrl.push("ProfileEditPage", { user_details: this.from_login });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Profile</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="background">\n  \n<!-- <div class="img-circular">url("../assets/icon/male.png");</div><br> -->\n<div class="padding-icon">\n  <img src="assets/icon/male.png" width="30%" />\n</div>\n\n<ion-list *ngFor="let item of user_details">\n\n<p class="down1">{{item.first_name}} {{item.last_name}}</p> \n\n</ion-list>\n\n\n<ion-item *ngFor="let item of user_details" >\n<ion-row class="container">\n<ion-col col-9>\n  <h2 style=" color: #781318; font-weight: bolder;">Profile Details</h2>\n  <ion-item no-lines> <p class="font_style" text-wrap>Fullname: {{item.first_name}} {{item.last_name}}</p></ion-item>\n  <ion-item no-lines> <p class="font_style" text-wrap>Mobile Number: {{item.mobile_number}}</p></ion-item>\n  <ion-item no-lines> <p class="font_style" text-wrap>Email: {{item.email}}</p></ion-item>\n\n</ion-col>\n<ion-col col-3>\n  <button ion-button color="primary" outline (tap)="edit(item)"> Edit</button>\n</ion-col>\n</ion-row>\n</ion-item>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

});
//# sourceMappingURL=8.js.map