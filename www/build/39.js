webpackJsonp([39],{

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CateringItemDetailsPageModule", function() { return CateringItemDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__catering_item_details__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CateringItemDetailsPageModule = /** @class */ (function () {
    function CateringItemDetailsPageModule() {
    }
    CateringItemDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__catering_item_details__["a" /* CateringItemDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__catering_item_details__["a" /* CateringItemDetailsPage */]),
            ],
        })
    ], CateringItemDetailsPageModule);
    return CateringItemDetailsPageModule;
}());

//# sourceMappingURL=catering-item-details.module.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CateringItemDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CateringItemDetailsPage = /** @class */ (function () {
    function CateringItemDetailsPage(apis, cartServ, loadingCtrl, alertCtrl, http, navCtrl, navParams) {
        var _this = this;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fakeUsers = new Array(5);
        this.user_details = this.navParams.get("user_details");
        this.restaurant_id = this.navParams.get("restaurant_id");
        this.meals_id = this.navParams.get("meals_id");
        this.meals_id_jsonBody = {
            "meals_id": this.meals_id
        };
        console.log('meals_id ID retrieved:', this.meals_id);
        this.apis.meal_details(this.meals_id_jsonBody).then(function (result) {
            _this.body = result;
            _this.meals_details = JSON.stringify(_this.body);
            console.log("Lets see all the meals_details " + _this.meals_details);
        });
        console.log("-----------------------------------");
        this.count = this.cartServ.getAllCateringCartItems().length;
        console.log("FIRST COUNT " + this.count);
    }
    CateringItemDetailsPage.prototype.cart = function () {
        this.navCtrl.push("CartsPage", { user_details: this.user_details });
    };
    CateringItemDetailsPage.prototype.addTocart = function (item) {
        console.log("These are the items " + item);
        console.log("MEAL QUANTITYY " + item.quantity);
        var pro = JSON.stringify(item);
        console.log("NOw strign " + pro);
        this.cartServ.addItem(item, item.quantity);
        console.log(this.cartServ.getAllCateringCartItems());
        this.count = this.cartServ.getAllCateringCartItems().length;
        console.log("WHAT IS COUNT " + this.count);
        this.navCtrl.push("CateringMenuPage", { restaurant_id: this.restaurant_id, user_details: this.user_details });
    };
    CateringItemDetailsPage.prototype.quantityAdd = function (item) {
        this.cartServ.quantityPlus(item);
    };
    CateringItemDetailsPage.prototype.quantityMinus = function (item) {
        if (item.quantity > 1) {
            this.cartServ.quantityMinus(item);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Quantity is 1, you cant reduce it.',
                buttons: ['Ok']
            });
            alert_1.present();
        }
    };
    CateringItemDetailsPage.prototype.removeItemFromCart = function (item) {
        //this.cartService.removeItemById(item.id);
        var self = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure you want to remove food item from cart?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: function () {
                        console.log('Buy clicked');
                        self.cartServ.removeItemById(item.id);
                    }
                }
            ]
        });
        alert.present();
    };
    CateringItemDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-catering-item-details',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/catering-item-details/catering-item-details.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Item Details</ion-title>\n\n    <ion-buttons end class="margin">\n      <button ion-button icon-only (tap)="cart()">\n        <!-- <img src="assets/imgs/cart.png" height="30px" width="30px" class="right" /> -->\n        <ion-icon name="cart" class="right"></ion-icon>\n        <ion-badge>{{count}}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="background">\n  <ion-list *ngFor="let item of body">\n    <ion-item class="center">\n      <!-- <img src="assets/imgs/papaye.jpg" height="100px" width="150px" /> -->\n      <img [src]="item.meal_logo_url" height="100px" width="150px" />\n    </ion-item>\n\n    <ion-item>\n      <ion-list>\n        <h2>\n          <b style="color: black">{{item.meal_name}}</b>\n        </h2>\n        <p>{{item.description}}</p>\n        <p>Ghc {{item.price}}</p>\n      </ion-list>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-list>\n        <p>\n          <b style="color: black">Quantity</b>\n        </p>\n      </ion-list>\n      <ion-row>\n        <ion-col col-4 tappable>\n          <ion-icon\n            ios="ios-remove-circle"\n            md="md-remove-circle"\n            color="danger"\n            (tap)="quantityMinus(item)"\n          ></ion-icon>\n        </ion-col>\n\n        <ion-col col-4>\n          <h4 class="margin">{{item.quantity}}</h4>\n        </ion-col>\n        <ion-col col-4 tappable>\n          <ion-icon\n            ios="ios-add-circle"\n            md="md-add-circle"\n            color="secondary"\n            (tap)="quantityAdd(item)"\n          ></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <!-- <ion-item>\n    <ion-label style="color: black">Your Choice</ion-label>\n    <ionic-selectable item-content [(ngModel)]="port" itemValueField="id" itemTextField="name" [items]="ports">\n    </ionic-selectable>\n  </ion-item> -->\n\n    <ion-item no-lines>\n      <h2>Special Request (optional)</h2>\n    </ion-item>\n    <ion-item>\n      <ion-input type="text" placeholder="Type here..."></ion-input>\n    </ion-item>\n\n    <ion-item no-lines>\n      <!-- <ion-row>\n\n      <ion-col col-3 tappable>\n        <button id="font" ion-button color="kaya" (tap)="quantityMinus(item)">\n          <ion-icon ios="ios-remove" md="md-remove"></ion-icon>\n        </button>\n      </ion-col>\n\n      <ion-col col-3>\n        <h4 class="margin">\n          <b>{{item.quantity}}</b>\n        </h4>\n      </ion-col>\n      <ion-col col-3 tappable>\n        <button id="font" ion-button color="secondary"  (tap)="quantityAdd(item)">\n          <ion-icon ios="ios-add" md="md-add" ></ion-icon>\n        </button>\n      </ion-col>\n\n\n    </ion-row> -->\n    </ion-item>\n\n    <ion-footer>\n      <p>\n        <button\n          ion-button\n          color="secondary"\n          class="button button-dark customBtn"\n          (tap)="addTocart(item)"\n        >\n          Add To Cart\n        </button>\n      </p>\n    </ion-footer>\n  </ion-list>\n\n  <ion-list *ngIf="!body">\n    <ion-list-header\n      *ngFor="let fake of fakeUsers; let i = index"\n      no-lines\n      no-padding\n      class="fakeItem"\n    >\n      <ion-list no-lines>\n        <ion-item class="center">\n          <img [src]="" />\n        </ion-item>\n\n        <ion-item>\n          <ion-list>\n            <h2>\n              <b style="color: black"></b>\n            </h2>\n            <p></p>\n            <p></p>\n          </ion-list>\n        </ion-item>\n      </ion-list>\n    </ion-list-header>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/catering-item-details/catering-item-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["c" /* CateringCartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], CateringItemDetailsPage);
    return CateringItemDetailsPage;
}());

//# sourceMappingURL=catering-item-details.js.map

/***/ })

});
//# sourceMappingURL=39.js.map