webpackJsonp([11],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentHistoryPageModule", function() { return PaymentHistoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_history__ = __webpack_require__(358);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentHistoryPageModule = /** @class */ (function () {
    function PaymentHistoryPageModule() {
    }
    PaymentHistoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_history__["a" /* PaymentHistoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_history__["a" /* PaymentHistoryPage */]),
            ],
        })
    ], PaymentHistoryPageModule);
    return PaymentHistoryPageModule;
}());

//# sourceMappingURL=payment-history.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaymentHistoryPage = /** @class */ (function () {
    function PaymentHistoryPage(navCtrl, modalCtrl, navParams, apis, cartServ, loadingCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.apis = apis;
        this.cartServ = cartServ;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.order_history = "Meals";
        this.user_details = this.navParams.get("customer_details");
        //Checking if user data is shown / logged in
        if (this.user_details == undefined) {
            console.log("No Login - NO User Data");
        }
        else {
            this.body = this.user_details;
            this.jsonBody = JSON.parse(this.body);
            this.user_id = this.jsonBody[0].id;
            console.log("USer DETAILS " + this.user_details);
            console.log("USer ID " + this.user_id);
            this.params = {
                "user_id": this.user_id
            };
            var loader_1 = this.loadingCtrl.create({
                content: "Please wait ...",
            });
            loader_1.present();
            this.apis.payment_history(this.params).then(function (result) {
                console.log(result);
                _this.paymentJson = JSON.stringify(result);
                console.log('LETS SEE THE PROCESS ORDER ' + _this.paymentJson);
                _this.list = JSON.parse(_this.paymentJson);
                loader_1.dismiss();
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Sorry, cant connect right now. Please try again!",
                    buttons: ['OK']
                });
                alert.present();
                loader_1.dismiss();
            });
        }
    }
    PaymentHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment-history',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/payment-history/payment-history.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Payment History</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="background">\n\n\n  <ion-list *ngIf="list?.length == 0">\n    <p class="textstyle"> No Payment History Found </p>\n  </ion-list>\n\n\n  <ion-list *ngFor="let item of list">\n\n    <!-- <ion-card> -->\n      <ion-item>\n        <ion-avatar item-start *ngIf=" item.mobile_wallet_network == \'mtn\'">\n\n          <img src="assets/icon/mtn.png" />\n\n        </ion-avatar>\n        <ion-avatar item-start *ngIf=" item.mobile_wallet_network == \'vodafone\'">\n\n          <img src="assets/icon/voda.png" />\n\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf=" item.mobile_wallet_network == \'airtel\'">\n\n          <img src="assets/icon/airtel.png" />\n\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf=" item.mobile_wallet_network == \'tigo\'">\n\n          <img src="assets/icon/tigo.png" />\n\n        </ion-avatar>\n\n        <h2 text-wrap>\n          Reference ID: {{ item.reference}}\n        </h2>\n        <h2 text-wrap>\n          OrderID: {{ item.order_id}}\n        </h2>\n        <h2 text-wrap>\n          Amount: GHc {{ item.amount}}\n        </h2>\n\n        <h2 text-wrap>\n          Status: {{ item.pay_status}}\n        </h2>\n        <!-- <h2 text-wrap *ngIf=" item.status == true">\n          Status:Payment Successful\n        </h2>\n        <h2 text-wrap *ngIf=" item.status == false">\n          Status: Pending...\n          </h2> -->\n        <h2 text-wrap>\n          Date: {{ item.created_at | date: \'medium\'}}\n        </h2>\n\n      </ion-item>\n    <!-- </ion-card> -->\n\n    <!-- <ion-card-content>\n\n      <ion-grid>\n        <ion-row>\n\n          <ion-col col-2 *ngIf=" item.mobile_wallet_network == \'mtn\'">\n            <h4>\n              <img src="assets/icon/mtn.png" height="100px" width="100px" />\n            </h4>\n          </ion-col>\n          <ion-col col-2 *ngIf=" item.mobile_wallet_network == \'airtel\'">\n            <h4>\n              <img src="assets/icon/airtel.png" height="100px" width="100px" />\n            </h4>\n          </ion-col>\n          <ion-col col-2 *ngIf=" item.mobile_wallet_network == \'vodafone\'">\n            <h4>\n              <img src="assets/icon/voda.png" height="100px" width="800px" />\n            </h4>\n          </ion-col>\n          <ion-col col-2 *ngIf=" item.mobile_wallet_network == \'tigo\'">\n            <h4>\n              <img src="assets/icon/tigo.png" height="100px" width="100px" />\n            </h4>\n          </ion-col>\n\n          <ion-col col-10>\n            <ion-item text-wrap>\n\n              <h4>Transacton ID: {{ item.reference}}</h4>\n              <h4>Order ID: {{ item.order_id}}</h4>\n              <h4>Amount: {{ item.amount}}</h4>\n              <h4>Transactiion Date: {{ item.created_at | date: \'medium\'}}</h4>\n            </ion-item>\n          </ion-col>\n\n        </ion-row>\n      </ion-grid>\n\n    </ion-card-content> -->\n  </ion-list>\n\n\n</ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/payment-history/payment-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["b" /* CartService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], PaymentHistoryPage);
    return PaymentHistoryPage;
}());

//# sourceMappingURL=payment-history.js.map

/***/ })

});
//# sourceMappingURL=11.js.map