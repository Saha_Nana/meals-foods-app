webpackJsonp([17],{

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(352);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(toastCtrl, apis, _form, loadingCtrl, alertCtrl, navCtrl, navParams, http) {
        this.toastCtrl = toastCtrl;
        this.apis = apis;
        this._form = _form;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.submitAttempt = false;
        this.from_cart_or_sidemenu = this.navParams.get("cart_or_sidemenu");
        console.log("From Sidemenu or cart " + this.from_cart_or_sidemenu);
        this.loginForm = this._form.group({
            "password": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            "mobile_number": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])]
        });
    }
    LoginPage.prototype.signup = function () {
        this.navCtrl.push("SignupPage");
    };
    LoginPage.prototype.reset = function () {
        this.navCtrl.push("ForgotpasswordPage");
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.loginVal = JSON.stringify(this.loginForm.value);
        console.log("LETS SEE THE LOGIN VAL " + this.loginVal);
        this.jsonBody = JSON.parse(this.loginVal);
        console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody);
        console.log("THIS IS THE SIGNUP VALUES STRINGIFY" + JSON.stringify(this.jsonBody));
        var loader = this.loadingCtrl.create({
            content: "Please wait ...",
            spinner: 'dots',
        });
        loader.present();
        // Retrieving customer details to pass in to the next page
        this.apis.retrieve_details(this.jsonBody).then(function (result) {
            _this.body1 = result;
            // body1 = JSON.parse(body1);
            _this.retrieved = _this.body1;
            _this.retrieved = JSON.stringify(_this.body1);
            console.log('LETS SEE THE  BODY OF RETRIEVAL ' + _this.body1);
            console.log('LETS SEE THE DATA RETRIEVED ' + _this.retrieved);
        });
        this.apis.login(this.jsonBody).then(function (result) {
            console.log(result);
            var jsonBody = result;
            console.log(jsonBody);
            // jsonBody = JSON.parse(jsonBody);
            // console.log(jsonBody)
            var desc = jsonBody["resp_desc"];
            var code = jsonBody["resp_code"];
            var message = jsonBody["message"];
            console.log(desc);
            console.log(code);
            _this.messageList = message;
            _this.api_code = code;
            loader.dismiss();
            if (_this.api_code == "200") {
                var loader_1 = _this.loadingCtrl.create({
                    content: "Login processing..."
                });
                loader_1.present();
                setTimeout(function () {
                    if (_this.from_cart_or_sidemenu == "sidemenu") {
                        _this.navCtrl.setRoot("HomePage", { customer_details: _this.retrieved });
                    }
                    else if (_this.from_cart_or_sidemenu == "cart") {
                        _this.navCtrl.push("FoodCartPage", { user_details: _this.retrieved });
                    }
                    else if (_this.from_cart_or_sidemenu == "meals_cart") {
                        _this.navCtrl.push("CartsPage", { user_details: _this.retrieved });
                    }
                    else if (_this.from_cart_or_sidemenu == "catering_cart") {
                        _this.navCtrl.push("CateringCartPage", { user_details: _this.retrieved });
                    }
                    else {
                        _this.navCtrl.setRoot("HomePage", { customer_details: _this.retrieved });
                    }
                }, 1000);
                setTimeout(function () {
                    loader_1.dismiss();
                }, 1000);
            }
            if (_this.api_code != "200") {
                var alert_1 = _this.alertCtrl.create({
                    title: "",
                    subTitle: "Wrong mobile number or password.Kindly try again!",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: "",
                subTitle: "Sorry, cant connect right now. Please try again!",
                buttons: ['OK']
            });
            alert.present();
            _this.toastCtrl.create({
                message: "Please check your internet connection",
                duration: 5000
            }).present();
            loader.dismiss();
            console.log(err);
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/saha/Ionic/mealfood/src/pages/login/login.html"*/'<!-- <ion-header>\n  <ion-navbar>\n\n    <ion-title>\n      <p>Login</p>\n    </ion-title>\n\n  </ion-navbar>\n</ion-header> -->\n<ion-content overflow-scroll=\'true\' class="background">\n  <br/>\n  <br/>\n  <br/>\n  <br/>\n  <br/>\n  <form [formGroup]="loginForm" class="move_form">\n\n    <ion-list class="main-view">\n      <br/>\n      <br/>\n      <br/>\n      <br/>\n      <ion-item>\n        <ion-input type="number" placeholder="Mobile number" formControlName="mobile_number" name="mobile_number" [class.invalid]="!loginForm.controls.mobile_number.valid && (loginForm.controls.mobile_number.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon ios="ios-phone-portrait" md="md-phone-portrait"></ion-icon>\n        </ion-label>\n        <ion-label> </ion-label>\n      </ion-item>\n      <br/>\n\n      <ion-item>\n\n        <ion-input type="password" placeholder="Password" formControlName="password" name="password" [class.invalid]="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)"></ion-input>\n        <ion-label>\n          <ion-icon name="ios-unlock"></ion-icon>\n        </ion-label>\n\n        <ion-label> </ion-label>\n      </ion-item>\n\n      <br/>\n      <p>\n        <button ion-button color="kaya" class="button button-dark customBtn" [disabled]="!this.loginForm.valid" (tap)="login()">\n          Login</button>\n      </p>\n      \n\n      <p class="pad">\n          <b style=" color: #187034;">\n              <p style=" color: #187034;">\n                  <a  (tap)="reset()" tappable style=" color: #187034;">Forgot Password?\n                  </a>\n              </p>\n          </b>\n      </p>\n\n    </ion-list>\n\n    <!-- <ion-footer> -->\n\n\n      <p class="pad">\n        <b style=" color: #32db64;">\n            <p style=" color: #32db64;">Don\'t have an account ?\n                <a (tap)="signup()" tappable style=" color: white;">Sign Up\n                </a>\n            </p>\n        </b>\n    </p>\n\n    <!-- </ion-footer> -->\n\n\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/saha/Ionic/mealfood/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_apis_apis__["a" /* ApisProvider */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=17.js.map